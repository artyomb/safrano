###########
# config.ru
#

$LOAD_PATH << '../../lib/'

require './dbmodel_run.rb'

# docu for Builder --> https://www.tuicool.com/articles/yyIFri

final_odata_app = Rack::OData::Builder.new do
  use Rack::CommonLogger
  use Rack::ShowExceptions
  run ODataSailBatch.new

end

Rack::Handler::WEBrick.run final_odata_app, Port: 9595
