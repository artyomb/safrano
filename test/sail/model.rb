
class Edition < Sequel::Model(:Edition)
end

class PeriodicityType < Sequel::Model(:PeriodicityType)
end

class Race < Sequel::Model(:Race)
  # A Race has a RaceType; there are many Races for a given type...
  # --> many_to_one
#  many_to_one :RaceType, key: :type
  many_to_one :rtype, class: :RaceType, key: :type

  # A Race has a CrewType; there are many Races for a given type...
  #--> many_to_one
  many_to_one :CrewType, key: :crew_type

  # A Race has a Periodicity; there are many Races for a given
  # type...--> many_to_one
  many_to_one :PeriodicityType, key: :periodicity

  # This allows to ask for  /Race(1)/Edition
  one_to_many :Edition, class: Edition, key: :race_id
end

# example of 'dynamically' added fields. TODO Document this (as a HOWTO?)
class RaceExtended < Race
  
  def values
    h = super
    h[:depth] = depth
    h
  end
  def depth
    Time.now.to_i
  end
  
  def self.columns
    c = Race.columns
    c.push :depth
  end
  def self.db_schema
    s = Race.db_schema.dup
    s[:depth] = {
      :generated=>false,
      :allow_null=>true,
      :default=>nil,
      :db_type=>"INTEGER",
      :primary_key=>false,
      :type=>:integer,
      :ruby_default=>nil}
    s
  end
end

class RaceType < Sequel::Model(:RaceType)
end

class BoatClass < Sequel::Model(:BoatClass)
end


# this is the assignment table for many to many Crew-Person(Member)
class CrewMember < Sequel::Model(:CrewMember)
end

class Person < Sequel::Model(:Person)
  # a person can be in many crews
  many_to_many :crews, left_key: :person_id, right_key: :crew_id , join_table: :CrewMember, class: :Crew
end

class Crew < Sequel::Model(:Crew)
  # a crew is composed of many members(persons)
  many_to_many :members, left_key: :crew_id, right_key: :person_id, join_table: :CrewMember, class: :Person
end

class CrewType < Sequel::Model(:CrewType)
end


class Edition
  # A Edition is for a given Race; there are many Editions for a
  # given Race...--> many_to_one
  many_to_one :Race, key: :race_id
end

class Ranking < Sequel::Model(:Ranking)
  # A Ranking is for a given Edition; there are many Rankings for a
  # given Edition...--> many_to_one
  many_to_one :Edition, key: %i[race_id race_num]
  many_to_one :Crew, key: :crew 
  many_to_one :Race, key: :race_id
end

### SERVER Part #############################

class ODataSailApp < Safrano::ServerApp
  publish_service do

    title  'Sail OData API'
    name  'SailService'
    namespace  'ODataSail'
    server_url 'http://example.org'
    path_prefix '/'

    publish_model RaceType
    publish_model Crew do add_nav_prop_collection :members end
    publish_model CrewMember
    publish_model CrewType
    publish_model BoatClass
    publish_model PeriodicityType, :periodicity_type
    publish_model Race do

      # This allows to ask for  /Race(1)/rtype with type RaceType
      add_nav_prop_single :rtype

      # This allows to ask for  /Race(1)/CrewType
      add_nav_prop_single :CrewType

      # This allows to ask for  /Race(1)/PeriodicityType
      add_nav_prop_single :PeriodicityType

    # This allows to ask for  /Race(1)/Edition
      add_nav_prop_collection :Edition
    end
    publish_model Person do add_nav_prop_collection :crews end

    # This allows to ask for  /Edition(...)/Race
    publish_model Edition do add_nav_prop_single :Race end

    publish_model Ranking do 
      add_nav_prop_single :Edition 
      add_nav_prop_single :Crew
      add_nav_prop_single :Race
    end
  end
end



