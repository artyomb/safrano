BEGIN TRANSACTION;
DROP TABLE IF EXISTS "Edition";
CREATE TABLE IF NOT EXISTS "Edition" (
	"race_id"	INTEGER NOT NULL,
	"num"	INTEGER NOT NULL,
	"year"	INTEGER NOT NULL,
  "start" DATETIME,
	"organizer"	TEXT,
	PRIMARY KEY("race_id","num"),
	FOREIGN KEY("race_id") REFERENCES "Race"("id")
);
DROP TABLE IF EXISTS "Ranking";
CREATE TABLE IF NOT EXISTS "Ranking" (
	"race_id"	INTEGER NOT NULL,
	"race_num"	INTEGER NOT NULL,
	"boat_class"	INTEGER NOT NULL,
	"rank"	INTEGER NOT NULL,
	"exaequo"	INTEGER,
	"crew"	INTEGER NOT NULL,
  "arrival" DATETIME,
	FOREIGN KEY("race_id","race_num","boat_class") REFERENCES "Issue",
	FOREIGN KEY("boat_class") REFERENCES "BoatClass"("id"),
	PRIMARY KEY("race_id","race_num","boat_class","rank","exaequo")
);
DROP TABLE IF EXISTS "Issue";
CREATE TABLE IF NOT EXISTS "Issue" (
	"race_id"	INTEGER NOT NULL,
	"race_num"	INTEGER NOT NULL,
	"boat_class"	INTEGER NOT NULL,
	"participants"	integer,
	"abandonments"	integer,
	FOREIGN KEY("boat_class") REFERENCES "BoatClass"("id"),
	FOREIGN KEY("race_id","race_num") REFERENCES "Edition",
	PRIMARY KEY("race_id","race_num","boat_class")
);
DROP TABLE IF EXISTS "CrewMember";
CREATE TABLE IF NOT EXISTS "CrewMember" (
	"crew_id"	INTEGER NOT NULL DEFAULT 1,
	"person_id"	INTEGER NOT NULL DEFAULT 1,
	FOREIGN KEY("crew_id") REFERENCES "Crew"("id"),
	FOREIGN KEY("person_id") REFERENCES "Person"("id"),
	PRIMARY KEY("crew_id","person_id")
);
DROP TABLE IF EXISTS "Person";
CREATE TABLE IF NOT EXISTS "Person" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"first_name"	TEXT,
	"last_name"	TEXT
);
DROP TABLE IF EXISTS "BoatClass";
CREATE TABLE IF NOT EXISTS "BoatClass" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"name"	TEXT
);
DROP TABLE IF EXISTS "Race";
CREATE TABLE IF NOT EXISTS "Race" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"name"	TEXT,
	"type"	INTEGER,
	"periodicity"	INTEGER,
	"crew_type"	INTEGER,
	FOREIGN KEY("type") REFERENCES "RaceType"("id"),
	FOREIGN KEY("crew_type") REFERENCES "CrewType"("id"),
	FOREIGN KEY("periodicity") REFERENCES "PeriodicityType"("id")
);
DROP TABLE IF EXISTS "RaceType";
CREATE TABLE IF NOT EXISTS "RaceType" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"description"	TEXT
);
DROP TABLE IF EXISTS "Crew";
CREATE TABLE IF NOT EXISTS "Crew" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"type"	INTEGER,
	FOREIGN KEY("type") REFERENCES "CrewType"("id")
);
DROP TABLE IF EXISTS "CrewType";
CREATE TABLE IF NOT EXISTS "CrewType" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"size"	INTEGER,
	"description"	TEXT
);
DROP TABLE IF EXISTS "PeriodicityType";
CREATE TABLE IF NOT EXISTS "PeriodicityType" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"description"	TEXT,
	"period"	INTEGER
);
DROP INDEX IF EXISTS "idx_racetype";
CREATE INDEX IF NOT EXISTS "idx_racetype" ON "Race" (
	"type"
);
DROP VIEW IF EXISTS "V_Ranking";
CREATE VIEW V_Ranking AS SELECT Race.name AS race_name, Edition.year AS year, Race.id, Edition.num, boat_class, BoatClass.name AS boat_class_name, rank, exaequo, arrival,
Person.first_name AS first_name, Person.last_name AS last_name
FROM Ranking inner join Race on Ranking.race_id = Race.id
 inner join Edition on Ranking.race_id = Edition.race_id and Ranking.race_num = Edition.num
 inner join BoatClass on Ranking.boat_class = BoatClass.id
 inner join CrewMember on Ranking.crew = CrewMember.crew_id
 inner join Person on CrewMember.person_id = Person.id;
DROP VIEW IF EXISTS "V_Edition";
CREATE VIEW V_Edition AS SELECT Edition.race_id, (Race.name || " " || Edition.year) as name_yr, Edition.num as num, start FROM Edition INNER JOIN Race ON Race.id = Edition.race_id;
DROP VIEW IF EXISTS "V_Crew";
CREATE VIEW V_Crew AS SELECT
  Crew.id as id,
  CrewType.description as type_descr,
    group_concat(Person.last_name) as member_name
from Crew
    inner join CrewType on Crew.type = CrewType.id
    inner join CrewMember on CrewMember.crew_id = Crew.id
    inner join Person on CrewMember.person_id = Person.id
group by Crew.id;
COMMIT;
