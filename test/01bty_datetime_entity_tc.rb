
require_relative './test_botypes_db.rb' # Botypes-DB testdata
require 'base64'

# Find a named timezone that has currently a given offset (in format +02:00 ) 
# regardless of DST and whatever

def find_named_timezone_by_offset(offs)
  TZInfo::Timezone.all.find{|tz| tz.to_local(DateTime.now).zone == offs }
end

module DateTime_TestMethod_Template
  def template_test_datetime(edm_json, expected, expected_zone: "+00:00",
                                    comment: nil, id: nil,
                                    method: :post, field: :datetimetz )
    @data = {}
    assert_false id.nil?, "Please provide an id>0 for method #{:method}" unless method == :post 
    
    @data['id'] = id unless method == :post 
    
    @data[field.to_s] = edm_json  # '/Date(1657874225000+120)/'   # 2022-07-15T10:37:05+02:00
    @data['comment'] = comment # 'Inserted DateTime 2022-07-15T10:37:05+02:00 '
    
    pbody = @data.to_json

    header 'Accept', 'application/json'
    header 'Content-Type', 'application/json'
        
    case method
    when :post
      post '/DBTypeDateTime', pbody
    when :put
      put "/DBTypeDateTime(#{id})", pbody
    end
    if method == :post
        assert_last_resp_created_one('DBTypeDateTime')
        @idnew = @jresd[:id]
    else # update response
        assert last_response.successful?, "Status of #{method} #{field} #{edm_json} incorrect: #{last_response.status}"
        assert last_response.no_content?, " #{method} #{field} #{edm_json} does not return with 'no content' " 
        @idnew = id
    end


    if method == :post
# check the returned id is a new one
        assert_false @idlist_old.include?(@idnew)
    end

#check created data
    DBTypeDateTime[@idnew].reload
    actual = DBTypeDateTime[@idnew].send(field)
    assert_equal expected, actual, "Result of #{method} #{field} #{edm_json} incorrect"
    unless field == :date
      assert_equal expected_zone, actual.zone, "Resulting Timezone of #{method} #{field} #{edm_json} incorrect"
    end
  end 
end

# Things that work same in sqlite and Postgres
class DateTime_AllDBTypes_TC < BotypesRackTest
  include DateTime_TestMethod_Template
  def setup
    super
    @idlist_old = DBTypeDateTime.to_a.map{|r| r.id}
  end  
  def test_get
     # SQL DATE type colum
    get "/DBTypeDateTime(1)"
    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    assert_equal @jrawd[:edm_json], @jrawd[:date]
    
    # SQL DATETIME / timestamp wo TZ type colum
    get "/DBTypeDateTime(2)"
    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    
    assert_equal @jrawd[:edm_json], @jrawd[:datetime]   
  end
  
  # more or less insert copy or record #1 with a simple date
  def test_create_update_date

   # Insert; Provide and check the DATETIMETZ column
    template_test_datetime('/Date(1657843200000)/', 
                            Date.new(2022,07,15),
                            field: :date, 
                            comment: 'Inserted Date 2022-07-15')

    # Same but with a PUT update ; add a second
    template_test_datetime('/Date(0)/', 
                            Date.new(1970,01,01),
                            field: :date,
                            method: :put, id: @idnew,
                            comment: 'Updated to  Date 1970-01-01 ') 

  end 
  
  # more or less insert copy or record #2 with a  datetime without TZ
  # /Date(1657874225000)/	Time.utc(2022,07,15,8,37,5) 
  def test_create_update_datetime_wo_tz


    # Insert; Provide and check the DATETIME column
    template_test_datetime('/Date(1657874225000)/', 
                            DateTime.new(2022,07,15,8,37,5,0), 
                            field: :datetime,
                            comment: 'Inserted DateTime 2022-07-15T08:37:05Z ')

    # Same but with a PUT update ; add a second
    template_test_datetime('/Date(1657874226000)/', 
                            DateTime.new(2022,07,15,8,37,6,0), 
                            field: :datetime,
                            method: :put, id: @idnew,
                            comment: 'Updated DateTime 2022-07-15T08:37:06Z ') 

  end   
end

# Note: for consistency this uses all DBTypeDateTime created with +02:00 offset as input
# and we use the same application_timezone
class DateTime_FixedOffset_Plus_two_TC < BotypesRackTest
  include DateTime_TestMethod_Template
  def self.startup
    super
    Sequel.application_timezone = find_named_timezone_by_offset('+02:00')

    # this would also work?
    #Sequel.application_timezone = nil
    #Sequel.typecast_timezone =   find_named_timezone_by_offset('+02:00')
  end

  def setup
    super
    @idlist_old = DBTypeDateTime.to_a.map{|r| r.id}
  end  
  
  def test_get

    #ensure the Sequel TZ settings are correct
    assert_equal :utc, Sequel.database_timezone
    assert_equal  '+02:00', Sequel.application_timezone.to_local(DateTime.now).zone 
    
    get "/DBTypeDateTime(3)"

    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    assert_equal @jrawd[:edm_json], @jrawd[:datetimetz] 
 
    get "/DBTypeDateTime(4)"
    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    assert_equal @jrawd[:edm_json], @jrawd[:datetimetz] 

    # Note: trying to test with records 5 and 6 does not work as these were inputed with
    # offset +-4  but here we say application_timezone = +2
    
    get "/DBTypeDateTime(7)"
    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    assert_equal @jrawd[:edm_json], @jrawd[:datetimetz]       
  end
  
# more or less insert copy or record #3 with a  datetime with TZ +02:00
  # /Date(1657874225000+120)/	2022-07-15T10:37:05+02:00
  def test_create_update_datetime_with_tz
    return unless Sequel::Model.db.adapter_scheme == :sqlite

    # Insert; Provide and check the DATETIMETZ column (field parameter has defult :datetimetz)
    template_test_datetime('/Date(1657874225000+120)/', 
                            DateTime.new(2022,07,15,10,37,5,'+02:00'), 
                            expected_zone: '+02:00',
                            comment: 'Inserted DateTime 2022-07-15T10:37:05+02:00 ')

    # Same but with a PUT update ; add a second
    template_test_datetime('/Date(1657874226000+120)/', 
                            DateTime.new(2022,07,15,10,37,6,'+02:00'), 
                            expected_zone: '+02:00', id: @idnew,
                            method: :put,
                            comment: 'Updated to DateTime 2022-07-15T10:37:06+02:00 ')    
  end     

end

# Same as above but with the +4 records and a +04:00 application_timezone
class DateTime_FixedOffset_Plus_four_TC < BotypesRackTest
  include DateTime_TestMethod_Template
  def self.startup
    super
    Sequel.application_timezone = find_named_timezone_by_offset('+04:00')
  end
  def setup
    super
    @idlist_old = DBTypeDateTime.to_a.map{|r| r.id}
  end  
  def test_get

    #ensure the Sequel TZ settings are correct
    assert_equal :utc, Sequel.database_timezone
    assert_equal  '+04:00', Sequel.application_timezone.to_local(DateTime.now).zone 
    
    get "/DBTypeDateTime(6)"
    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    assert_equal @jrawd[:edm_json], @jrawd[:datetimetz] 
    get "/DBTypeDateTime(8)"
    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    assert_equal @jrawd[:edm_json], @jrawd[:datetimetz]  
        
  end
# more or less insert copy or record #8 with a  datetime with TZ +04:00
  # /Date(1657874225000+240)/	2022-07-15T12:37:05+04:00
  def test_create_update_datetime_with_tz 
    
    # Insert; Provide and check the DATETIMETZ column
    template_test_datetime('/Date(1657874225000+240)/', 
                            DateTime.new(2022,07,15,12,37,5,'+04:00'), 
                            expected_zone: '+04:00',
                            comment: 'Inserted DateTime 2022-07-15T12:37:05+04:00 ')

    # Same but with a PUT update ; add a second
    template_test_datetime('/Date(1657874226000+240)/', 
                            DateTime.new(2022,07,15,12,37,6,'+04:00'), 
                            method: :put, id: @idnew,
                            expected_zone: '+04:00',
                            comment: 'Updated to DateTime 2022-07-15T12:37:06+04:00 ')     
    

  end       
  
end

# Postgresql Has Timestamp with TZ but it's not savin the TZ on DB; it's converting to UTC,
# and queries return the UTC DateTime (or a global DB-Timezone )
# ie. Postgresql does not "save" the input-timezone 

class DateTime_NoOffset_TC < BotypesRackTest
  # This runs with default setting Sequel.default_timezone=:utc
  include DateTime_TestMethod_Template
  def setup
    super
    @idlist_old = DBTypeDateTime.to_a.map{|r| r.id}
  end
  def test_get

    #ensure the Sequel TZ settings are all :utc
    assert_equal :utc, Sequel.database_timezone
    assert_equal :utc, Sequel.application_timezone
    
    get "/DBTypeDateTime(3)"
    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    assert_equal @jrawd[:edm_json_utc], @jrawd[:datetimetz]  
   
    get "/DBTypeDateTime(4)"
    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    assert_equal @jrawd[:edm_json_utc], @jrawd[:datetimetz]  

    get "/DBTypeDateTime(5)"

    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    assert_equal @jrawd[:edm_json_utc], @jrawd[:datetimetz] 

    get "/DBTypeDateTime(6)"
    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    assert_equal @jrawd[:edm_json_utc], @jrawd[:datetimetz] 
    
    get "/DBTypeDateTime(7)"
    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    assert_equal @jrawd[:edm_json_utc], @jrawd[:datetimetz] 
    get "/DBTypeDateTime(8)"
    assert_last_resp_enty_ok_no_cast(DBTypeDateTime)
    assert_equal @jrawd[:edm_json_utc], @jrawd[:datetimetz]       
  end
  
# more or less insert copy or record #8 with a  datetime with TZ +04:00
  # /Date(1657874225000+240)/	2022-07-15T12:37:05+04:00
  # But we have application tz = UTC
  def test_create_update_datetime_with_tz 


    # Insert; Provide and check the DATETIMETZ column
    template_test_datetime('/Date(1657874225000+240)/', 
                            DateTime.new(2022,07,15,8,37,5,0), 
                            expected_zone: '+00:00',
                            comment: 'Inserted DateTime 2022-07-15T12:37:05+04:00 ')

    # Same but with a PUT update ; add a second
    template_test_datetime('/Date(1657874226000+240)/', 
                            DateTime.new(2022,07,15,8,37,6,0), 
                            method: :put, id: @idnew,
                            expected_zone: '+00:00',
                            comment: 'Updated to DateTime 2022-07-15T12:37:06+04:00 ')     


  end         
end
