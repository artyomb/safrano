#!/usr/bin/env ruby

require 'test/unit'

require_relative '../lib/safrano/core_ext'


# https://www.epochconverter.com

class TC_ExtDateTimeFormat < TCWithoutDB
  def test_utc_time_to_edm_json
    t = Time.utc(2022,07,15,0,0,0)
    assert_equal '/Date(1657843200000)/', t.to_edm_json 

    t = Time.utc(2001,2,3,4,5,6)
    assert_equal '/Date(981173106000)/', t.to_edm_json 

    t = Time.utc(1951,2,3,4,5,6)
    assert_equal '/Date(-596750094000)/', t.to_edm_json     
  end
  def test_utc_time_from_edm_json

    assert_equal Time.utc(2022,07,15,0,0,0), Time.from_edm_json('/Date(1657843200000)/')
    assert_equal Time.utc(2001,2,3,4,5,6), Time.from_edm_json('/Date(981173106000)/')
    assert_equal Time.utc(1951,2,3,4,5,6), Time.from_edm_json('/Date(-596750094000)/')

   
  end
  def test_utc_datetime_to_edm_json
    # DateTime.new(...,0)  last 0 to ensúre UTC
    t = DateTime.new(2022,07,15,0,0,0,0)
    assert_equal '/Date(1657843200000)/', t.to_edm_json 

    t = DateTime.new(2001,2,3,4,5,6,0)
    assert_equal '/Date(981173106000)/', t.to_edm_json 

    t = DateTime.new(1951,2,3,4,5,6,0)
    assert_equal '/Date(-596750094000)/', t.to_edm_json     
  end
  def test_utc_datetime_from_edm_json

    assert_equal DateTime.new(2022,07,15,0,0,0,0), DateTime.from_edm_json('/Date(1657843200000)/')
    assert_equal DateTime.new(2001,2,3,4,5,6,0), DateTime.from_edm_json('/Date(981173106000)/')
    assert_equal DateTime.new(1951,2,3,4,5,6,0), DateTime.from_edm_json('/Date(-596750094000)/')

   
  end  
  def test_date_to_edm_json
    t = Date.new(2022,07,15)
    assert_equal '/Date(1657843200000)/', t.to_edm_json   
    
    t = Date.new(1951,2,3)
    assert_equal '/Date(-596764800000)/', t.to_edm_json
    
    t = Date.new(1970,1,1)
    assert_equal '/Date(0)/', t.to_edm_json

    t = Date.new(1970,1,2)
    assert_equal '/Date(86400000)/', t.to_edm_json  
    
    t = Date.new(1969,12,31)
    assert_equal '/Date(-86400000)/', t.to_edm_json       
  end

  def test_date_from_edm_json

    assert_equal Date.new(2022,07,15), Date.from_edm_json('/Date(1657843200000)/') 
    assert_equal Date.new(1951,2,3), Date.from_edm_json('/Date(-596764800000)/')
    assert_equal Date.new(1970,1,1), Date.from_edm_json('/Date(0)/')
    assert_equal Date.new(1970,1,2), Date.from_edm_json('/Date(86400000)/')
    assert_equal Date.new(1969,12,31), Date.from_edm_json('/Date(-86400000)/')
  end
  
  def test_offset_time_to_edm_json
    offset = '+02:00'  # This should result in 2*3600 seconds less in the "tics since utc epoch" 

    t = Time.new(2001,2,3,4,5,6, offset)
    assert_equal '/Date(981165906000+120)/', t.to_edm_json 

    t = Time.new(1951,2,3,4,5,6, offset)
    assert_equal '/Date(-596757294000+120)/', t.to_edm_json  

    offset = '-03:00'  # This should result in 3*3600 seconds more in the "tics since utc epoch" 
    t = Time.new(2001,2,3,4,5,6, offset)
    assert_equal    '/Date(981183906000-180)/', t.to_edm_json  
  end
  def test_offset_time_from_edm_json
    offset = '+02:00'  # This should result in 2*3600 seconds less in the "tics since utc epoch" 
    assert_equal Time.new(2001,2,3,4,5,6, offset), Time.from_edm_json('/Date(981165906000+120)/') 
    assert_equal Time.new(1951,2,3,4,5,6, offset), Time.from_edm_json('/Date(-596757294000+120)/') 
    

    offset = '-03:00'  # This should result in 3*3600 seconds more in the "tics since utc epoch" 
    assert_equal Time.new(2001,2,3,4,5,6, offset), Time.from_edm_json('/Date(981183906000-180)/') 
 
  end
  def test_offset_datetime_edm_json
    offset = '+02:00'
    
    t = DateTime.new(2001,2,3,4,5,6,offset)
    assert_equal '/Date(981165906000+120)/', t.to_edm_json 

    t = DateTime.new(1951,2,3,4,5,6,offset)
    assert_equal '/Date(-596757294000+120)/', t.to_edm_json     
    
    offset = '-03:00'  # This should result in 3*3600 seconds more in the "tics since utc epoch" 
    t = DateTime.new(2001,2,3,4,5,6, offset)
    assert_equal    '/Date(981183906000-180)/', t.to_edm_json      
  end  

  def test_offset_datetime_from_edm_json
    offset = '+02:00'  # This should result in 2*3600 seconds less in the "tics since utc epoch" 
    assert_equal DateTime.new(2001,2,3,4,5,6, offset), DateTime.from_edm_json('/Date(981165906000+120)/') 
    assert_equal DateTime.new(1951,2,3,4,5,6, offset), DateTime.from_edm_json('/Date(-596757294000+120)/') 
    

    offset = '-03:00'  # This should result in 3*3600 seconds more in the "tics since utc epoch" 
    assert_equal DateTime.new(2001,2,3,4,5,6, offset), DateTime.from_edm_json('/Date(981183906000-180)/') 
 
  end  
end
