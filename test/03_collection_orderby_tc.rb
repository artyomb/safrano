#!/usr/bin/env ruby
ENV['RACK_ENV'] = 'test'

require_relative './test_saildb.rb' # Sail-DB testdata

module TM_Orderby_Asserts

  def get_orderby_resp(eset= 'Race')
    o = URI.encode_www_form_component @orderby
    if @adpar
      a = URI.encode_www_form_component @adpar
      get "#{eset}?$orderby=#{o}&$filter=#{a}"
    else
      get "#{eset}?$orderby=#{o}"
    end
  end
  
  
  def expect_asc(eset='Race', &proc)
    get_orderby_resp(eset)
    assert_last_resp_coll_parseable_ok(eset, @expected_tab.count)
    assert_sequel_coll_set_equal(@expected_tab, @jresd_coll)
    assert_sequel_coll_ord_asc_by(@jresd_coll, &proc)
  end
 
  def expect_desc(eset='Race', &proc)
    get_orderby_resp(eset)
    assert_last_resp_coll_parseable_ok(eset, @expected_tab.count)
    assert_sequel_coll_set_equal(@expected_tab, @jresd_coll)
    assert_sequel_coll_ord_desc_by(@jresd_coll, &proc)
  end
  
end

module TM_Orderby
  
  def test_basic_order_0
    @orderby = "name"
    expect_asc {|jr| jr[:name] }
  end

  def test_basic_order_1
    @orderby = "name asc"
    expect_asc {|jr| jr[:name] }
  end

  def test_basic_order_2
    @orderby = "name desc"
    expect_desc {|jr| jr[:name] }
  end

  def test_basic_order_0i
    @orderby = "name,id"
    expect_asc {|jr| [jr[:name], jr[:id]] }
  end

  def test_basic_order_1i
    @orderby = "name asc, id"
    expect_asc {|jr| [jr[:name], jr[:id]] }
  end

  def test_basic_order_2i
    @orderby = "name desc,id"
    expect_desc {|jr| [jr[:name], (0-jr[:id])] }
  end
  
 
  

end

module TM_RK_Orderby

  # Ranking sorted by Race/name
  # this failed up to v0.4.2 because of an error in collection#attribute_path_list
  def test_rk_orderby_race_name
    @orderby = "Race/name"
    expect_desc('Ranking') {|jrk| jrk[:Race][:name] }
  end
  
end

class RKCollectionOrderbyTest < SailDBTest
 def setup
    @jresd = nil
    @jresd_coll = nil
    @adpar = nil
    @expected_tab = Ranking.all
  end
  include TM_Orderby_Asserts
  include TM_RK_Orderby
end
 
class CollectionOrderbyTest < SailDBTest
  def setup
    @jresd = nil
    @jresd_coll = nil
    @adpar = nil
    @expected_tab = Race.all
  end
  include TM_Orderby_Asserts
  include TM_Orderby
end

# Same tests should pass with additional filtering
class CollectionOrderbyAndFilterTest < SailDBTest
  def setup
    @jresd = nil
    @jresd_coll = nil
    @adpar = 'crew_type eq 1 or type eq 1'
    @expected_tab = Race.all.select{|r| r.crew_type == 1 or r.type == 1 }
  end
  include TM_Orderby_Asserts
  include TM_Orderby
end

# Same tests should pass with additional filtering
class CollectionOrderbyAndMoreFilterTest < SailDBTest
  def setup
    @jresd = nil
    @jresd_coll = nil
    @adpar = "name ne 'Oryx Quest' and id ge 2"
    @expected_tab = Race.all.select{|r| r.name != 'Oryx Quest' && r.id >= 2 }
  end
  include TM_Orderby_Asserts
  include TM_Orderby
end