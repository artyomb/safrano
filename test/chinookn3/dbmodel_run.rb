#!/usr/bin/env ruby
$LOAD_PATH << '../../lib/'
require_relative '../../lib/safrano.rb'


require_relative  '../run_helper'

require 'sequel'

  $DB = {}

dbrname = File.join(__dir__, 'chinookn3_test.db3')
$DB[:chinookn3] = Sequel::Model.db = Sequel.sqlite(dbrname)

at_exit {
 Sequel::Model.db.disconnect if Sequel::Model.db 
 }

require_relative './model.rb'
