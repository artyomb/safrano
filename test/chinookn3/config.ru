###########
# config.ru
#

$LOAD_PATH << '../../lib/'

require_relative './dbmodel_run.rb'

puts "Safrano version #{Safrano::VERSION  }"


# docu for Builder --> https://www.tuicool.com/articles/yyIFri

final_odata_app = Rack::Safrano::Builder.new do
  run ODataChinookApp.new
end

run final_odata_app


