
# for testing Media API error we need a dummy model not used elsewhere
# --> NotAMedia 
create_dummy_tables('botanics')

class NotAMedia < Sequel::Model(:not_a_media)
end

# put this in a own Namspace to avoid clasches with the non-API test model classes
module API00Test
  class Cultivar < Sequel::Model(:cultivar)
    # Navigation attributes
    one_to_many :parent, class: :Parentage, key: :cultivar_id
    one_to_many :child, class: :Parentage, key: :parent_id
  
    many_to_one :breeder, key: :breeder_id
  end
  
  class Breeder < Sequel::Model(:breeder)
    one_to_many :cultivar, key: :breeder_id
  end
  
  class Parentage < Sequel::Model(:parentage)
    # A parentage is for a given cultivar; there can be multiple parentage records
    # (type: father or mother), or one (type seedling) or none
    #
    many_to_one :child, class: :Cultivar, key: :cultivar_id
    many_to_one :parent, class: :Cultivar, key: :parent_id
    many_to_one :par_type, class: :ParentageType, key: :ptype_id
  end
end
# some class not derived from Sequel::Model()
class NonModelClass
end
