require 'test/unit'

require_relative '../lib/odata/filter/parse.rb'

class TCFilterWithoutDB < TCWithoutDB
  include Safrano::Filter
  def assert_ast_tree(expt)
    assert_kind_of RootTree, @ast.result
    assert_equal expt, @ast.result
  end
  def assert_leave_val_equal(expected)
    assert_equal expected, @ast.result.first_child_value, 'Filter-Expr Parser: First child leave value incorrect'
  end
  def assert_ast_error(expeklass)
    assert @ast.error
    assert (@ast.error.class == expeklass) or 
           (@ast.error.is_a? expeklass)
  end
end

class FilterParseBasics < TCFilterWithoutDB


  def test_lit_node
    @ast =  Parser.new('axy').build
    expt = RootTree.new do attach Literal.new('axy') end
    assert_ast_tree(expt)
  end

  def test_qualit_node
    @ast =  Parser.new('a/xy').build
    expt = RootTree.new do attach Qualit.new('a/xy') end
    assert_ast_tree(expt)
  end

  def test_qstring_node
    @ast =  Parser.new("'axy'").build
    expt = RootTree.new do attach QString.new('axy') end
    assert_ast_tree(expt)
  end

  def test_qstring_escape_quote_node
    
    @ast =  Parser.new("'axy''quoted'''").build
    
    expt = RootTree.new do attach QString.new("axy'quoted'") end
    assert_ast_tree(expt)
  end
  
  def test_datetime_node
    @ast =  Parser.new("datetime'2000-12-12T12:00:53'").build
    expt = RootTree.new do attach DateTimeLit.new('2000-12-12T12:00:53') end
    assert_ast_tree(expt)
    assert_leave_val_equal '2000-12-12T12:00:53'

  end
  
  def test_datetimeoffset_node
    @ast =  Parser.new("datetimeoffset'2000-12-12T12:00:53+02:00'").build
    expt = RootTree.new do attach DateTimeOffsetLit.new('2000-12-12T12:00:53+02:00') end
    assert_ast_tree(expt)
    assert_leave_val_equal '2000-12-12T12:00:53+02:00'
  end
  
  def test_fpnumber_node
    
    @ast =  Parser.new('5').build
    
    expt = RootTree.new do attach FPNumber.new('5') end
    assert_ast_tree(expt)

    
    @ast =  Parser.new('5.12').build
    expt = RootTree.new do attach FPNumber.new('5.12') end
    assert_ast_tree(expt)
    assert_leave_val_equal '5.12'
    
    @ast =  Parser.new('5.12f').build
    expt = RootTree.new do attach FPNumber.new('5.12') end
    assert_ast_tree(expt)
    assert_leave_val_equal '5.12'    
    
    @ast =  Parser.new('5.12d').build
    expt = RootTree.new do attach FPNumber.new('5.12') end
    assert_ast_tree(expt)
    assert_leave_val_equal '5.12'     
    
    
    @ast =  Parser.new('5.12m').build
    expt = RootTree.new do attach DecimalLit.new('5.12') end
    assert_ast_tree(expt)
    assert_leave_val_equal '5.12'
    
    @ast =  Parser.new('3.1415e1').build
    expt = RootTree.new do attach FPNumber.new('3.1415e1') end
    assert_ast_tree(expt)

    
    @ast =  Parser.new('3.1415e+1').build
    expt = RootTree.new do attach FPNumber.new('3.1415e+1') end
    assert_ast_tree(expt)

    
    @ast =  Parser.new('3.1415e-1').build
    expt = RootTree.new do attach FPNumber.new('3.1415e-1') end
    assert_ast_tree(expt)

  end

  def test_func_2_args
    
    @ast =  Parser.new('concat(a,b)').build
    # instance_eval magic....
    expt = RootTree.new do
      attach FuncTree.new(:concat){
        attach ArgTree.new('('){
          attach Literal.new 'a'
          update_state(',', :Separator)
          attach Literal.new 'b'
          update_state(')', :Delimiter)
        }
      }

    end
    assert_ast_tree(expt)
  end

  def test_func_2_args_lit_qstring
    
    @ast =  Parser.new(%Q<concat(a,'b')>).build
    # instance_eval magic....
    expt = RootTree.new do
      attach FuncTree.new(:concat){
        attach ArgTree.new('('){
          attach Literal.new 'a'
          update_state(',', :Separator)
          attach QString.new 'b'
          update_state(')', :Delimiter)
        }
      }

    end
    assert_ast_tree(expt)
  end

  def test_binops_expr_eq
    
    @ast =  Parser.new('a EQ b').build
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopBool.new(:eq){
            attach Literal.new 'a'
            attach Literal.new 'b'
          }

    end
    assert_ast_tree(expt)
  end
  def test_binops_expr_deci_eq
    
    @ast =  Parser.new('Total EQ 8.91m').build
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopBool.new(:eq){
            attach Literal.new 'Total'
            attach DecimalLit.new '8.91'
          }

    end
    assert_ast_tree(expt)
  end
  

  def test_binops_expr_arith
    
    @ast =  Parser.new('a add b').build
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopTree.new(:add){
            attach Literal.new 'a'
            attach Literal.new 'b'
          }

    end
    assert_ast_tree(expt)
  end

  def test_binops_expr_le
    
    @ast =  Parser.new('a Le b').build
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopTree.new(:le){
            attach Literal.new 'a'
            attach Literal.new 'b'
          }

    end
    assert_ast_tree(expt)
  end
  def test_datetime_binops_expr_le
    
    @ast =  Parser.new("a Le daTetime'2000-12-12T12:00'").build
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopTree.new(:le){
            attach Literal.new 'a'
            attach DateTimeLit.new '2000-12-12T12:00'
          }

    end
    assert_ast_tree(expt)
  end
  def test_binops_expr_gt_in_parenthesis
    
    @ast =  Parser.new('(a GT b)').build
    # instance_eval magic....
    expt = RootTree.new do
          attach IdentityFuncTree.new{
            attach ArgTree.new('(') {
              attach BinopTree.new(:gt){
                attach Literal.new 'a'
                attach Literal.new 'b'
            }
            update_state(')', :Delimiter)
          }
        }
    end
    assert_ast_tree(expt)
  end

  def test_concat_func_eql_exp
    
    @ast =  Parser.new('concat(a,b) EQ x').build
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopTree.new(:eq){
            attach FuncTree.new(:concat){
              attach ArgTree.new('('){
                attach Literal.new 'a'
                update_state(',', :Separator)
                attach Literal.new 'b'
                update_state(')', :Delimiter)
              }
            }
            attach Literal.new 'x'
          }

    end
    assert_ast_tree(expt)
  end

  def test_trim_func_eql_exp
    
    @ast =  Parser.new(%Q<trim(a) EQ 'xyz'>).build
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopTree.new(:eq){
            attach FuncTree.new(:trim){
              attach ArgTree.new('('){
                attach Literal.new 'a'
                update_state(')', :Delimiter)
              }
            }
            attach QString.new 'xyz'
          }

    end
    assert_ast_tree(expt)
  end

  def test_not_expr
    
      @ast =  Parser.new('not true').build
    # instance_eval magic....
    expt = RootTree.new do
          attach UnopTree.new(:not){
            attach Literal.new 'true'
          }

    end
    assert_ast_tree(expt)
  end

  def test_not_not_expr
    
      @ast =  Parser.new('not not true').build
    # instance_eval magic....
    expt = RootTree.new do
          attach UnopTree.new(:not){
            attach UnopTree.new(:not){
              attach Literal.new 'true'
            }
          }

    end
    assert_ast_tree(expt)
  end

  def test_not_func_expr
    
      @ast =  Parser.new(%Q<not endswith(a,'name')>).build
    # instance_eval magic....
    expt = RootTree.new do
          attach UnopTree.new(:not){
            attach FuncTree.new(:endswith){
              attach ArgTree.new('('){
                attach Literal.new 'a'
                update_state(',', :Separator)
                attach QString.new 'name'
                update_state(')', :Delimiter)
              }
            }
          }

    end
    assert_ast_tree(expt)
  end

  # now it's not basics anymore
end

class FilterParseLogical < TCFilterWithoutDB

 

  ## is this correct? normally NOT has higher precedence as EQ, thus
  ## is should read as (not concat(a,b)) EQ x) but concat does return a string
  ## --> should be a typ error ?
  #def test_not_func_eql_exp
    #
      #@ast =  Parser.new('not concat(a,b) EQ x').build
    #end
    ## instance_eval magic....
    #expt = RootTree.new do
        #attach UnopTree.new(:not){
          #attach BinopTree.new(:eq){
            #attach FuncTree.new(:concat){
              #attach ArgTree.new('('){
                #attach Literal.new 'a'
                #update_state(',', :Separator)
                #attach Literal.new 'b'
                #update_state(')', :Delimiter)
              #}
            #}
            #attach Literal.new 'x'
          #}
    #}
    #end
    ##assert_ast_tree(expt)
  #end

  # mul/div has higher preced as add/sub
  def test_arithm_mul_preced_sub
    # should eval as (a*b) - 5
    
      @ast =  Parser.new('a mul b sub 5').build
    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:sub){
            attach BinopTree.new(:mul){
              attach Literal.new 'a'
              attach Literal.new 'b'
              update_state('b', :Literal)
          }
          attach FPNumber.new '5'
          update_state('5', :FPNumber)
        }
    end
    assert_ast_tree(expt)
  end

  # arithm has higher preced as comparison
  def test_arithm_preced
    # should eval as (a+b) gt c
    
      @ast =  Parser.new('a add b GT 5').build
    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:gt){
            attach BinopTree.new(:add){
              attach Literal.new 'a'
              attach Literal.new 'b'
              update_state('b', :Literal)
          }
          attach FPNumber.new '5'
          update_state('5', :FPNumber)
        }
    end
    assert_ast_tree(expt)
  end

  # precedence not > relational(le,gt...) > and
  def test_unop_gt

    # should eval as (not x) eq b
    
      @ast =  Parser.new('not x eq b').build
    # instance_eval magic....
    expt = RootTree.new do
      attach BinopTree.new(:eq){
        attach UnopTree.new(:not){
          attach Literal.new('x')
      }
      attach Literal.new('b')
      update_state('b', :Literal)
    }
    end
    assert_ast_tree(expt)
  end

  # check and > or precedence
  def test_binops_expr_and_or

    
      @ast =  Parser.new('a and b or c').build

    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:or){
            attach BinopTree.new(:and){
              attach Literal.new 'a'
              attach Literal.new 'b'
              update_state('b', :Literal)
          }
          attach Literal.new 'c'
          update_state('c', :Literal)
        }
    end
    assert_ast_tree(expt)
  end

  def test_binops_expr_or_and

    
      @ast =  Parser.new('a or b and c').build

    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:or){
            attach Literal.new 'a'
            attach( ba = BinopTree.new(:and){
              attach Literal.new 'b'
              attach Literal.new 'c'
              update_state('c', :Literal)
          })
          update_state(ba, :BinopTree)
        }

    end
    assert_ast_tree(expt)
  end

  # precedence not > relational(le,gt...) > and
  def test_unop_binop_expr_and_gt

    # should eval as (not x) and (a gt 5)
    
      @ast =  Parser.new('not x and a gt 5').build
    # instance_eval magic....
    expt = RootTree.new do
      attach BinopTree.new(:and){
        attach UnopTree.new(:not){
          attach Literal.new('x')
      }
      attach BinopTree.new(:gt){
        attach Literal.new('a')
        attach FPNumber.new('5')
        update_state('5', :FPNumber)
      }
    }
    end
    assert_ast_tree(expt)
  end

  # precedence relational(le,gt...) > and
  def test_binops_expr_le_and_gt_2

    
      @ast =  Parser.new('a le 200 and a gt 5').build

    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:and){
            attach BinopTree.new(:le){
            attach Literal.new 'a'
            attach FPNumber.new '200'
            update_state('200', :FPNumber)
          }
          attach BinopTree.new(:gt){
          attach Literal.new 'a'
          attach FPNumber.new '5'
          update_state('5', :FPNumber)
        }
      }
    end
    assert_ast_tree(expt)
  end

  def test_binops_expr_le_and_gt_3

    # according to precedence, this is grouped  as
    # ' ((a le 200) and (a gt 5)) or (b eq 100)'
    
      @ast =  Parser.new('a le 200 and a gt 5 or b eq 100').build

    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:or){
      attach BinopTree.new(:and){
            attach BinopTree.new(:le){
            attach Literal.new 'a'
            attach FPNumber.new '200'
          }
          attach BinopTree.new(:gt){
          attach Literal.new 'a'
          attach FPNumber.new '5'
        }
      }
      attach BinopTree.new(:eq){
        attach Literal.new 'b'
        attach FPNumber.new '100'
      }
    }

    end
    assert_equal expt.children.first, @ast.result.children.first
  end

  def test_binops_expr_func_eq_or_2

    
      @ast =  Parser.new(%Q<toupper(a) eq 'ME' or toupper(a) eq 'AND'>).build

    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:or){
            attach BinopTree.new(:eq){
              attach FuncTree.new(:toupper){
                attach ArgTree.new('('){
                  attach Literal.new 'a'
                  update_state(')', :Delimiter)
                }
              }
            attach QString.new 'ME'
            }
            attach BinopTree.new(:eq){
              attach FuncTree.new(:toupper){
                attach ArgTree.new('('){
                  attach Literal.new 'a'
                  update_state(')', :Delimiter)
                }
              }
            attach QString.new 'AND'
            }
          }
    end
    assert_ast_tree(expt)
  end

  # same precedences (or) --> uses leftmost
  def test_binops_expr_func_eq_or_3

    
      @ast =  Parser.new(%Q<toupper(a) eq 'ME' or toupper(b) eq 'AND' or toupper(c) eq 'YOU'>).build

    ta = BinopTree.new(:eq){
      attach FuncTree.new(:toupper){
        attach ArgTree.new('('){
          attach Literal.new 'a'
          update_state(')', :Delimiter)
        }
      }
      attach QString.new 'ME'
      update_state('ME', :QString)
      }

      tb = BinopTree.new(:eq){
        attach FuncTree.new(:toupper){
          attach ArgTree.new('('){
            attach Literal.new 'b'
            update_state(')', :Delimiter)
          }
        }
        attach QString.new 'AND'
        update_state('AND', :QString)
      }

       tc = BinopTree.new(:eq){
        attach FuncTree.new(:toupper){
          attach ArgTree.new('('){
            attach Literal.new 'c'
            update_state(')', :Delimiter)
          }
        }
      attach QString.new 'YOU'
      update_state('YOU', :QString)
      }

    # instance_eval magic....
    expt = RootTree.new do
    attach BinopTree.new(:or){
      attach BinopTree.new(:or){
         attach ta
         attach tb
         update_state(tb, :BinopTree)
         }
      attach tc
      update_state(tc, :BinopTree)
      }
    end
    assert_ast_tree(expt)
  end

end

class FilterParseNested < TCFilterWithoutDB


  def test_concat_nested_eql
    
      inp = %Q<concat(firstname,concat(' ', lastname)) EQ 'Larry Joplin'>
      @ast =  Parser.new(inp).build
    # instance_eval magic....
    expt = RootTree.new do
          attach BinopTree.new(:eq){
            attach FuncTree.new(:concat){
              attach ArgTree.new('('){
                attach Literal.new 'firstname'
                @state = accept?(',', :Separator)
                attach FuncTree.new(:concat){
                  attach ArgTree.new('('){
                  attach QString.new ' '
                  update_state(',', :Separator)
                  attach Literal.new 'lastname'
                  update_state(')', :Delimiter)
              }
                }
                update_state(')', :Delimiter)
              }
            }
            attach QString.new 'Larry Joplin'
          }

    end
    assert_ast_tree(expt)
  end
  
end
class FilterParseParenthesisNested  < TCFilterWithoutDB

  # empty parenthesis do nothing
  def test_idempotent_1
    
    @ast =  Parser.new('( )').build
        
    # instance_eval magic....
    expt = RootTree.new do
      attach IdentityFuncTree.new{
        attach ArgTree.new('(') {
          update_state(')', :Delimiter)
          }
        }
    end
      
    assert_ast_tree(expt)
      
    @ast =  Parser.new('()()').build
    expt = RootTree.new do
      attach IdentityFuncTree.new{
        attach ArgTree.new('(') {
          update_state(')', :Delimiter)
          }
        attach ArgTree.new('(') {
          update_state(')', :Delimiter)
          }
        }  
    end
      
    assert_ast_tree(expt)
  end
  
  # empty nested parenthesis do nothing
  def test_idempotent_nest
    
    
    @ast =  Parser.new('(())').build
    assert_kind_of RootTree, @ast.result
    assert_nil @ast.error
    
    @ast =  Parser.new('(((())))').build
    assert_kind_of RootTree, @ast.result
    assert_nil @ast.error
  end
  
  # empty nested parenthesis do nothing
  def test_idempotent_nest_exp
    
    
    @ast =  Parser.new('((a eq 1))').build
    assert_kind_of RootTree, @ast.result
    assert_nil @ast.error
    
    @ast =  Parser.new('((((((((((((((((be ne 2))))))))))))))))').build
    assert_kind_of RootTree, @ast.result
    assert_nil @ast.error
  end
  
end

class FilterParseBasicErrors < TCFilterWithoutDB

  def test_single_closing_parenth
    @ast =  Parser.new(')').build
    assert_ast_error  Parser::ErrorUnmatchedClose
  end

  def test_unmatched_closing_parenth
    @ast =  Parser.new('(a EQ b))').build
    assert_ast_error  Parser::ErrorUnmatchedClose
  end

  def test_unmatched_quote
    @ast =  Parser.new("startswith(name,B')").build
    assert_ast_error  Parser::UnmatchedQuoteError
  end

  def test_unmatched_and
    @ast =  Parser.new("AND name eq 'test'").build
    assert_ast_error  Parser::ErrorInvalidToken
  end



  def test_two_seps
    @ast =  Parser.new('concat(a,,b)').build
    assert_ast_error  Parser::ErrorInvalidToken
  end

  def test_invalid_token_fp
    
    @ast =  Parser.new('3.1415d-1').build
    assert_ast_error  Parser::ErrorInvalidToken

    
    @ast =  Parser.new('3.1415x-1').build
    assert_ast_error  Parser::ErrorInvalidToken

    
    @ast =  Parser.new('3,1415').build
    assert_ast_error Parser::ErrorInvalidSeparator
  end

  def test_two_lits
    
    @ast =  Parser.new('a b').build
    assert_ast_error  Parser::ErrorInvalidToken
  end
  
  
  def test_func_arity
    
    @ast =  Parser.new('toupper(a,b)').build
    assert_ast_error Parser::ErrorInvalidArity
    
    
    @ast =  Parser.new('concat(a,b,c)').build
    assert_ast_error Parser::ErrorInvalidArity
    
    
    @ast =  Parser.new('concat(a)').build
    assert_ast_error Parser::ErrorInvalidArity
  end

  # a parenthesis expression should not contain a list
  def test_parenth_expr_arity
    
    @ast =  Parser.new('(a,b)').build
    assert_ast_error Parser::ErrorInvalidArity

    # but it can contain a single (eg. logical) expression
    
    @ast =  Parser.new('(a EQ b)').build
    assert_kind_of RootTree, @ast.result
    
    
    @ast =  Parser.new('(a EQ 3.14)').build
    assert_kind_of RootTree, @ast.result

  end

  # we dont have yet a full type check, because the type
  # of literals is on DB level... but we can at least
  # check that use with number raise an invalid type error
  def test_func_type_error
    
      @ast =  Parser.new('length(a)').build
    assert_kind_of RootTree, @ast.result

    
      @ast =  Parser.new('length(3.14)').build
    assert_ast_error Parser::ErrorInvalidArgumentType
  end

  def test_nested_mixed

    
      @ast =  Parser.new("concat(concat(first_name,' '),last_name)").build
    assert_kind_of RootTree, @ast.result

    
      @ast =  Parser.new("length(concat(concat(first_name,' '),last_name)) eq 10 ").build
    assert_kind_of RootTree, @ast.result

    #check with spaces
    
      @ast =  Parser.new("length(concat(concat(first_name, ' '), last_name)) eq 10 ").build
    assert_kind_of RootTree, @ast.result

  end
end
