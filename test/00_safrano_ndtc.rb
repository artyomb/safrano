#!/usr/bin/env ruby

require 'test/unit'

require_relative '../lib/safrano.rb'

class TC_Safrano < TCWithoutDB
  include Safrano

  ## test taht version is accessible
  def test_version
    assert_nothing_raised do
      puts "Safrano Version #{Safrano::VERSION}"
    end
  end

  ## TransitionEnd = Transition.new('\A(\/?)\z', trans:'transition_end')
  def test_transition_end
    TransitionEnd.do_match('')
    assert TransitionEnd.match_result
    assert_nil TransitionEnd.path_remain
    assert_equal('', TransitionEnd.path_done)

    TransitionEnd.do_match('/')
    assert TransitionEnd.match_result
    assert_nil TransitionEnd.path_remain
    assert_equal('/', TransitionEnd.path_done)

    TransitionEnd.do_match('xfdf')
    assert_nil TransitionEnd.match_result

    TransitionEnd.do_match('/x')
    assert_nil TransitionEnd.match_result
  end

  ##     TransitionMetadata = Transition.new('\A(\/\$metadata)(.*)', trans:'transition_metadata')
  def test_transition_metadata
    t = TransitionMetadata
    t.do_match('/$metadata')
    assert t.match_result
    assert_equal('', t.path_remain)
    assert_equal('/$metadata', t.path_done)

    t = TransitionMetadata
    t.do_match('/$metadata/')
    assert t.match_result
    assert_equal('/', t.path_remain)
    assert_equal('/$metadata', t.path_done)

    t = TransitionMetadata
    t.do_match('/$metatata')
    assert_nil t.match_result

    t = TransitionMetadata
    t.do_match('$metatata')
    assert_nil t.match_result
  end
end
