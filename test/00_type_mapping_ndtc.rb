#!/usr/bin/env ruby

require 'test/unit'

require_relative '../lib/safrano/type_mapping.rb'


    #with_db_type(/numeric|decimal/) do 
      #edm_type 'Edm.Decimal'
      #json_value ->(val){  output_as_BigDecimal(val) }
      
      #with_one_param{|param| 
        #edm_type "Edm.Decimal(#{param})"
        #json_value ->(val, parm){  output_as_Prec_BigDecimal(val, parm) }
      #}
      
      #with_two_params{|par1, par2| 
        #edm_type "Edm.Decimal(#{par1},#{par2})"
        #json_value ->(val, p1, p2){  output_as_Prec_BigDecimal(val, p1, p2) }
      #}
            
    #end

class TC_RgxTypeMapping < TCWithoutDB
  include Safrano
  def setup
    @builder = nil
  end
  
  def assert_matching(typnames)
    typnames.each{|typnam|
      @curtyp =   typnam 
      assert_kind_of RgxTypeMapping, @builder.match(@curtyp)
    } 

  end
  
  def assert_not_matching(othertypenames)
   
    othertypenames.each{|other| 
      assert_nil @builder.match(other)
    }
    
  end
  
  def test_create_fixed
    @builder = RgxTypeMapping.builder('typnam0') do
      edm_type 'Edm.Test'
      json_value ->(val){  "Test json value proc 0 params #{val}" }
    end
    assert_kind_of RgxTypeMapping::Builder, @builder
    assert_not_matching %w(foo Typenam) 
    assert_matching %w(typnam0 TyPnAm0)
    
    # we did  provide a value mapping 
    typmap = @builder.match(@curtyp)
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal  'Test json value proc 0 params foobar', typmap.castfunc.call('foobar')
  end  
  
  def test_create_fixed_edm_only
    @builder  = RgxTypeMapping.builder('typnam0') do
      edm_type 'Edm.Decimal'
    end
    
    assert_kind_of RgxTypeMapping::Builder, @builder
    assert_not_matching %w(foo Typenam) 
    assert_matching %w(typnam0 TyPnAm0)
    
    # we did not provide a value mapping proc/lamda. Thus should be nil
    assert_nil @builder.match(@curtyp).castfunc
  end
  
  def test_create_one_param

    @builder  = RgxTypeMapping.builder('typnam0') do
      edm_type 'Edm.Test0'
      json_value ->(val){  output_as_Decimal(val) }
      
      with_one_param{|param| 
        edm_type "Edm.Decimal(#{param})"
        json_value ->(val){  "Test json  proc 1 parm val(#{val}) parm(#{param})" }
      }
      
    end
    
    assert_kind_of RgxTypeMapping::Builder, @builder
    assert_not_matching %w(foo Typenam) 
    assert_matching %w(typnam0 TyPnAm0)
    
    # we did  provide a value mapping 
    typmap = @builder.match(@curtyp)
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal 'Edm.Test0', typmap.edm_type
    
    assert_not_matching %w<foo Typenam foo(22) Typenam(22)>
    assert_matching %w<typnam0(22) TyPnAm0(22)>
    typmap = @builder.match(@curtyp)

    assert_equal 'Edm.Decimal(22)', typmap.edm_type
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal  'Test json  proc 1 parm val(foobaz) parm(22)', typmap.castfunc.call('foobaz')    


    assert_matching %w<TyPnAm0(666)>
    typmap = @builder.match(@curtyp)

    assert_equal 'Edm.Decimal(666)', typmap.edm_type
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal  'Test json  proc 1 parm val(foobearz) parm(666)', typmap.castfunc.call('foobearz')    
 
  end  
  
  def test_create_two_param

    @builder  = RgxTypeMapping.builder('typnam0') do
      edm_type 'Edm.Test0'
      json_value ->(val){  output_as_Decimal(val) }
      
      with_two_params{|par1, par2| 
        edm_type "Edm.Test2(#{par1}, #{par2})"
        json_value ->(val){  "Test json  proc 2 parm val(#{val}) par1(#{par1}) par2(#{par2})" }
      }
      
    end
    
    assert_kind_of RgxTypeMapping::Builder, @builder
    assert_not_matching %w(foo Typenam) 
    assert_matching %w(typnam0 TyPnAm0)
    
    # we did  provide a value mapping 
    typmap = @builder.match(@curtyp)
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal 'Edm.Test0', typmap.edm_type
    
    assert_not_matching %w<foo Typenam foo(22) Typenam(22) typnamxx(1,5) TyPnAm0xx(5,10)>
    assert_not_matching %w<typnam0(22) TyPnAm0(22)>
    
    assert_matching %w<typnam0(1,5) TyPnAm0(5,10)>
    typmap = @builder.match(@curtyp)

    assert_equal 'Edm.Test2(5, 10)', typmap.edm_type
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal  'Test json  proc 2 parm val(foobaz) par1(5) par2(10)', typmap.castfunc.call('foobaz')    


    assert_matching [ 'TyPnAm0(666, 88)' ]
    typmap = @builder.match(@curtyp)

    assert_equal 'Edm.Test2(666, 88)', typmap.edm_type
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal  'Test json  proc 2 parm val(foobearz) par1(666) par2(88)', typmap.castfunc.call('foobearz')    
 
  end  
  
  def test_full

    @builder  = RgxTypeMapping.builder('typnam0') do
      edm_type 'Edm.Test0'
      json_value ->(val){  output_as_Decimal(val) }
      
      with_one_param{|param| 
        edm_type "Edm.Decimal(#{param})"
        json_value ->(val){  "Test json  proc 1 parm val(#{val}) parm(#{param})" }
      }
      
      with_two_params{|par1, par2| 
        edm_type "Edm.Test2(#{par1}, #{par2})"
        json_value ->(val){  "Test json  proc 2 parm val(#{val}) par1(#{par1}) par2(#{par2})" }
      }
      
    end
    assert_kind_of RgxTypeMapping::Builder, @builder
    assert_not_matching %w(foo Typenam) 
    assert_matching %w(typnam0 TyPnAm0)
    
    # we did  provide a value mapping 
    typmap = @builder.match(@curtyp)
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal 'Edm.Test0', typmap.edm_type
    
    assert_not_matching %w<foo Typenam foo(22) Typenam(22)>
    assert_matching %w<typnam0(22) TyPnAm0(22)>
    typmap = @builder.match(@curtyp)

    assert_equal 'Edm.Decimal(22)', typmap.edm_type
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal  'Test json  proc 1 parm val(foobaz) parm(22)', typmap.castfunc.call('foobaz')    


    assert_matching %w<TyPnAm0(666)>
    typmap = @builder.match(@curtyp)

    assert_equal 'Edm.Decimal(666)', typmap.edm_type
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal  'Test json  proc 1 parm val(foobearz) parm(666)', typmap.castfunc.call('foobearz')    
    

    
    assert_not_matching %w<foo Typenam foo(22) Typenam(22) typnamxx(1,5) TyPnAm0xx(5,10)>
    assert_matching %w<typnam0(1,5) TyPnAm0(5,10)>
    typmap = @builder.match(@curtyp)

    assert_equal 'Edm.Test2(5, 10)', typmap.edm_type
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal  'Test json  proc 2 parm val(foobaz) par1(5) par2(10)', typmap.castfunc.call('foobaz')    


    assert_matching [ 'TyPnAm0(666, 88)' ]
    typmap = @builder.match(@curtyp)

    assert_equal 'Edm.Test2(666, 88)', typmap.edm_type
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal  'Test json  proc 2 parm val(foobearz) par1(666) par2(88)', typmap.castfunc.call('foobearz')    
 
  end   
  def test_deci_num
    @builder  =  RgxTypeMapping.builder('numeric', 'decimal') do 
      edm_type 'Edm.Decimal'
      json_value ->(val){  "Numeric|Decimal param-free overriding #{val}" }
      
      with_one_param{|param| 
        edm_type "Edm.Decimal(#{param})"
        json_value ->(val){  "Numeric|Decimal one-param(#{param}) overriding #{val}"  }
      }
      
      with_two_params{|par1, par2| 
        edm_type "Edm.Decimal(#{par1},#{par2})"
        json_value ->(val){  "Numeric|Decimal two-param(#{par1}, #{par2}) overriding #{val}"  }
      }
            
    end
    assert_kind_of RgxTypeMapping::Builder, @builder
    assert_not_matching %w(foo Typenam) 
    assert_matching %w(DECIMAL  NUMERIC)
    typmap = @builder.match(@curtyp)
   
    assert_equal 'Edm.Decimal', typmap.edm_type
    assert typmap.castfunc
    assert typmap.castfunc.respond_to?(:call)
    assert_equal  'Numeric|Decimal param-free overriding foobaz', typmap.castfunc.call('foobaz') 
  end
  
end
