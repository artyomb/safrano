
require_relative './test_botanics_db.rb' # Botanics-DB testdata
require 'base64'

class EntityBoMultiPK_TC < BotanicsRackTest

# multi pk mixed types
  def test_multi_pk_mixed
    get "/Multi_PK(id=1,ids='bar')"
    assert_last_resp_entity_parseable_ok('Multi_PK')
    assert_sequel_values_equal(Multi_PK[1,'bar'], @jresd,
                               uri: "Multi_PK(id=1,ids='bar')")
                               
    get "/Multi_PK(ids='foo',id=2)"
    assert_last_resp_entity_parseable_ok('Multi_PK')
    assert_sequel_values_equal(Multi_PK[2,'foo'], @jresd,
                               uri: "Multi_PK(id=2,ids='foo')")                           
  end
  
  
  # multi pk mixed types
  def test_multi_pk_mixed_coll
    get '/Multi_PK'
    assert_last_resp_coll_parseable_ok('Multi_PK', Multi_PK.to_a.size)
    assert_metadata_uri(Multi_PK.first, @jresd_coll.first){|mpk| "Multi_PK(id=#{mpk.id},ids='#{mpk.ids}')"}
  end
  
 
  
end