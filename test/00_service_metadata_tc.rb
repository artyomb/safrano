#!/usr/bin/env ruby

require_relative './test_botanics_db.rb' # Botanics-DB testdata

class ServiceMetadataTC < BotanicsRackTest

  def setup
    # run only once.
    # we will just test different parts of the XML result
    # in each test
    #return if @xmlsch
    
    get '/$metadata'

    assert_equal "application/xml;charset=utf-8", 
                 last_response.content_type

    assert last_response.ok?

    @xml = REXML::Document.new(last_response.body)
    
    assert_equal('http://schemas.microsoft.com/ado/2007/06/edmx',
                 @xml.root.attributes['xmlns:edmx'])
                 
    assert_equal('Edmx', @xml.root.name)
    assert_equal('edmx', @xml.root.prefix)
    
    @xmlds = @xml.root.elements.first
    assert_equal 'DataServices', @xmlds.name
    
    @xmlsch = @xmlds.elements.first
    assert_equal 'Schema', @xmlsch.name
    
    
    assert @ec = @xmlsch.elements.find('EntityContainer'){|e| 
      e.attributes['Name'] == 'ParentageService'
    }
  end


  def test_it_has_metadata_entitytype

    
    assert cultivarTypeXml = @xmlsch.elements.find('EntityType'){|e| 
      e.attributes['Name'] == 'Cultivar'
    }

    cultivarProps = { id: 'Edm.Int32',
                      name: 'Edm.String',
                      year: 'Edm.Int32',
                      breeder_id: 'Edm.Int32',
                      inst_id: 'Edm.Int32'}
    
    cultivarProps.each{|name, type| 
      assert cultivarTypeXml.each_element('Property'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
            (   e.attributes['Type'] == type ) )
      }, "#{name}, #{type} not ok"

    }
    
    assert cultivarTypeXml.each_element('Key'){}.find{|e|
      e.each_element('PropertyRef'){}.find{|k| k.attributes['Name'] == 'id' }
    } 

    
  end
  

  
  def test_it_has_metadata_complex_type

    assert ct00Xml = @xmlsch.elements.find('ComplexType'){|e| 
      e.attributes['Name'] == 'ComplexType00'
      }

    ct00Props = { x: 'Edm.Int32', d: 'Edm.String'}
    
    ct00Props.each{|name, type| 
      assert ct00Xml.each_element('Property'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
            (   e.attributes['Type'] == type ) )
      }, "#{name}, #{type} not ok"

    }

    assert ct01Xml = @xmlsch.elements.find('ComplexType'){|e| 
      e.attributes['Name'] == 'ComplexType01'
    }

    ct01Props = { a: 'Edm.Int32', b: 'ODataPar.ComplexType00'}
    
    ct01Props.each{|name, type| 
      assert ct01Xml.each_element('Property'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
            (   e.attributes['Type'] == type ) )
      }, "#{name}, #{type} not ok"

    }

        
  end

  def test_it_has_metadata_function_import_tfunc
    
    
    assert tfunc = @ec.elements.find('FunctionImport'){|e| 
      e.attributes['Name'] == 'tfunc'}
    assert_equal 'ODataPar.ComplexType01', tfunc.attributes['ReturnType']
    assert_equal 'GET', tfunc.attributes['m:HttpMethod']
    
    
    tfuncParams = { x: 'Edm.Int32', d: 'Edm.String', a: 'Edm.Int32'}
    
    tfuncParams.each{|name, type| 
      assert tfunc.each_element('Parameter'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
              ( e.attributes['Type'] == type      )  and 
              ( e.attributes['Mode'] == 'In'      )  )
      }, " Param #{name}, #{type} not ok"

    }
  end

  def test_it_has_metadata_function_import_vfunc
    
    
    assert vfunc = @ec.elements.find('FunctionImport'){|e| 
      e.attributes['Name'] == 'vfunc'}
    assert_equal 'Edm.Double', vfunc.attributes['ReturnType']
    assert_equal 'GET', vfunc.attributes['m:HttpMethod']
    
    
    vfuncParams = { i: 'Edm.Int32', f: 'Edm.Double'}
    
    vfuncParams.each{|name, type| 
      assert vfunc.each_element('Parameter'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
              ( e.attributes['Type'] == type      )  and 
              ( e.attributes['Mode'] == 'In'      )  )
      }, "Param #{name}, #{type} not ok"

    }
  end
  
  def test_it_has_metadata_function_import_fcount
    assert func = @ec.elements.find('FunctionImport'){|e| 
      e.attributes['Name'] == 'fcount'}
    assert_equal 'Collection(Edm.String)', func.attributes['ReturnType']
    assert_equal 'GET', func.attributes['m:HttpMethod']
    
    
    funcParams = { i: 'Edm.Int32'}
    
    funcParams.each{|name, type| 
      assert func.each_element('Parameter'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
              ( e.attributes['Type'] == type      )  and 
              ( e.attributes['Mode'] == 'In'      )  )
      }, "Param #{name}, #{type} not ok"

    }
  end

  def test_it_has_metadata_function_import_CT_Coll
    assert func = @ec.elements.find('FunctionImport'){|e| 
      e.attributes['Name'] == 'ctfunc'}
    assert_equal 'Collection(ODataPar.ComplexType00)', func.attributes['ReturnType']
    assert_equal 'GET', func.attributes['m:HttpMethod']
    
    
    funcParams = { i: 'Edm.Int32', d: 'Edm.String' }
    
    funcParams.each{|name, type| 
      assert func.each_element('Parameter'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
              ( e.attributes['Type'] == type      )  and 
              ( e.attributes['Mode'] == 'In'      )  )
      }, "Param #{name}, #{type} not ok"

    }
  end

  def test_it_has_metadata_function_import_entity
    assert func = @ec.elements.find('FunctionImport'){|e| 
      e.attributes['Name'] == 'first_cultivar_func'}
    assert_equal 'ODataPar.Cultivar', func.attributes['ReturnType']
    assert_equal 'GET', func.attributes['m:HttpMethod']
    
    
    funcParams = { }
    
    funcParams.each{|name, type| 
      assert func.each_element('Parameter'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
              ( e.attributes['Type'] == type      )  and 
              ( e.attributes['Mode'] == 'In'      )  )
      }, "Param #{name}, #{type} not ok"

    }
  end

  def test_it_has_metadata_function_import_entity_coll
    assert func = @ec.elements.find('FunctionImport'){|e| 
      e.attributes['Name'] == 'top10_cultivar_func'}
    assert_equal 'Collection(ODataPar.Cultivar)', func.attributes['ReturnType']
    assert_equal 'GET', func.attributes['m:HttpMethod']
    
    
    funcParams = { }
    
    funcParams.each{|name, type| 
      assert func.each_element('Parameter'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
              ( e.attributes['Type'] == type      )  and 
              ( e.attributes['Mode'] == 'In'      )  )
      }, "Param #{name}, #{type} not ok"

    }
  end

  def test_it_has_metadata_navproperties
    
    assert cultivarTypeXml = @xmlsch.elements.find('EntityType'){|e| 
      e.attributes['Name'] == 'Cultivar'}

    cultivarRels = { institute: %w( Institute 0..1 ),
                     parentage: %w(Parentage *),
                     child_parentage: %w(Parentage *),
                     photo: %w(Photo *),
                     breeder: %w(Breeder 0..1)}
    
    cultivarRels.each{|name, props| 
      toRole = props[0]
      multpl = props[1]
      
      # check that we have all NavProperties
      assert nav_prop = cultivarTypeXml.elements.find('NavigationProperty'){|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
              ( e.attributes['ToRole'] == toRole ) )
      }
      
      # check that their referenced Association exists
      namespace, nav_prop_rel = nav_prop.attributes['Relationship'].split('.')
      assert_equal 'ODataPar', namespace
      
      assert ( assoc = @xmlsch.elements.find('Association'){|e| 
        e.attributes['Name'] == nav_prop_rel } )
      
      # check the multiplicity of the toRole End (ie. single/multiple )
      assert (toRoleProps = assoc.each_element('End'){}.find{|e| 
        e.attributes['Role'] == toRole})
      
      assert_equal multpl, toRoleProps.attributes['Multiplicity']
      
    }

    
  end


  def test_it_has_stream_attribute
    
    assert photoTypeXml = @xmlsch.elements.find('EntityType'){|e|
      e.attributes['Name'] == 'Photo'}
    
    assert_equal 'true', photoTypeXml.attributes['HasStream']

    assert cultivarTypeXml = @xmlsch.elements.find('EntityType'){|e| 
      e.attributes['Name'] == 'Cultivar'}

    assert_nil cultivarTypeXml.attributes['HasStream']
  end
  
end

