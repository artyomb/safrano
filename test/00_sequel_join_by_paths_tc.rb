#!/usr/bin/env ruby


require_relative './test_chinook_db.rb'

class OneLevel <  ChinookDBTestII
  # Album many to one Artist
  def test_many_to_one_1
    jh=Album.join_by_paths_helper('artist')
    aa = Album.get_alias_sym('artist')
    res = jh.dataset.where(Sequel[aa][:Name] => 'Led Zeppelin').to_a

    assert_nil res.find{|a| a.values[:Name] != 'Led Zeppelin'}

    # brute force check the returned set of AlbumId's
    exp_album_ids = Artist.where(:Name => 'Led Zeppelin').to_a.map{|artist|
      Album.where(:ArtistId => artist.ArtistId).to_a.map{|album| album.AlbumId}
    }.flatten.to_set

    assert_equal exp_album_ids, res.map{|album|album.AlbumId}.to_set
  end

  # Album many to one Artist
  def test_many_to_one_2
    jh=Album.join_by_paths_helper('artist')
# implicit alias: : Album.aliases_sym('artist') --> :a1
# but this depends on potentially run previous tests.... better call the alias method
    aa = Album.get_alias_sym('artist')
    res = jh.dataset.where(Sequel.like(Sequel[aa][:Name], 'The%')).to_a

    assert_nil res.find{|a| a.values[:Name] !~ /\AThe/ }

    # brute force check the returned set of AlbumId's
    exp_album_ids = Artist.where(Sequel.like(:Name, 'The%')).to_a.map{|artist|
      Album.where(:ArtistId => artist.ArtistId).to_a.map{|album| album.AlbumId}
    }.flatten.to_set

    assert_equal exp_album_ids, res.map{|album|album.AlbumId}.to_set
  end


end

class Deep <  ChinookDBTestII
  def test_customer_having_bought_albums_of_the
    jh = Customer.join_by_paths_helper('invoices/invoice_items/track/album/artist')
# implicit aliases:
#Customer.aliases_sym('invoices') --> :a1
#Customer.aliases_sym('invoices/invoice_items') --> :a2
#Customer.aliases_sym('invoices/invoice_items/track') --> :a3
#Customer.aliases_sym('invoices/invoice_items/track/album') --> :a4
#Customer.aliases_sym('invoices/invoice_items/track/album/artist') --> :a5
#
    aca = Customer.get_alias_sym('invoices/invoice_items/track/album/artist')
    res = jh.dataset.where(Sequel.like(Sequel[aca][:Name], 'The%')).to_a

    # Warning; this only works because
    # 1. column Name is only in artists tables otherwise we would have to qualify
    # 2. we did not specify the set of columns to select, so everything is returned
    assert_nil res.find{|c| c.values[:Name] !~ /\AThe/ }

    # brute force check the returned set of CustomerId's
    exp_customers_ids = Artist.where(Sequel.like(:Name, 'The%')).to_a.map{|artist|
      Album.where(:ArtistId => artist.ArtistId).to_a.map{|album|
        Track.where(:AlbumId => album.AlbumId).to_a.map{|track|
          InvoiceItem.where(:TrackId => track.TrackId).to_a.map{|item|
            Invoice.where(:InvoiceId => item.InvoiceId).to_a.map{|invoice|
              Customer.where(:CustomerId => invoice.CustomerId).to_a.map{|customer|
                customer.CustomerId
              }
            }
          }
        }
      }
    }.flatten.to_set

    assert_equal exp_customers_ids, res.map{|customer| customer.CustomerId}.to_set
  end

  def test_customer_having_bought_albums_of_the_sorted_by_salesrep_birthdate
    jh = Customer.join_by_paths_helper('invoices/invoice_items/track/album/artist')
    jh.add 'salesrep'
# implicit aliases:
#Customer.aliases_sym('invoices') --> :a1
#Customer.aliases_sym('invoices/invoice_items') --> :a2
#Customer.aliases_sym('invoices/invoice_items/track') --> :a3
#Customer.aliases_sym('invoices/invoice_items/track/album') --> :a4
#Customer.aliases_sym('invoices/invoice_items/track/album/artist') --> :a5
#Customer.aliases_sym('salesrep') --> :a6
    aca = Customer.get_alias_sym('invoices/invoice_items/track/album/artist')
    acs = Customer.get_alias_sym('salesrep')
    res = jh.dataset.where(Sequel.like(Sequel[aca][:Name], 'The%'))
                    .order(Sequel[acs][:BirthDate],
                    Sequel[Customer.table_name][:CustomerId]).to_a

    # Warning; this only works because
    # 1. column Name is only in artists tables otherwise we would have to qualify
    # 2. we did not specify the set of columns to select, so everything is returned
    assert_nil res.find{|c| c.values[:Name] !~ /\AThe/ }

    # brute force check the returned set of CustomerId's
    exp_customers_ids = {}
    Artist.where(Sequel.like(:Name, 'The%')).to_a.each{|artist|
      Album.where(:ArtistId => artist.ArtistId).to_a.each{|album|
        Track.where(:AlbumId => album.AlbumId).to_a.each{|track|
          InvoiceItem.where(:TrackId => track.TrackId).to_a.each{|item|
            Invoice.where(:InvoiceId => item.InvoiceId).to_a.each{|invoice|
              Customer.where(:CustomerId => invoice.CustomerId).to_a.each{|customer|
                exp_customers_ids[customer.CustomerId ] = customer.salesrep.BirthDate
              }
            }
          }
        }
      }
    }

    resh = {}
    res.each{|customer| resh[customer.CustomerId] = customer.salesrep.BirthDate}
    # expected:
    # sort by birthdate of salesrep (v), then by customerId (k)
    exp_customers_ids = exp_customers_ids.sort_by{|k,v| [v,k]}
    assert_equal exp_customers_ids, resh.to_a
  end

  #test a table linked twice to another table (employee) ; salesrep and techsupport
  def test_customer_techsupport_leaded_by_Bill

    jh = Customer.join_by_paths_helper('techsupport/repto')
# implicit aliases:
# Customer.aliases_sym('techsupport') --> :a7
# Customer.aliases_sym('techsupport/repto') --> :a8
    act = Customer.get_alias_sym('techsupport/repto')
    res = jh.dataset.where(Sequel[act][:FirstName] => 'Bill').to_a

     # brute force check the returned set of CustomerId's
    exp_customer_ids = Employee.where(:FirstName => 'Bill').to_a.map{|leadr|
      Employee.where(:ReportsTo => leadr.EmployeeId).to_a.map{|techsupport|
        Customer.where(:TechSupportId => techsupport.EmployeeId).to_a.map{|customer|
          customer.CustomerId
          }
      }
    }.flatten.to_set


    assert_equal exp_customer_ids, res.map{|customer| customer.CustomerId}.to_set

  end

  def test_customer_techsupport_leaded_by_Bill_sorted_by_salesrep_birthdate
# implicit aliases:
# Customer.aliases_sym('salesrep') --> :a6
# Customer.aliases_sym('techsupport') --> :a7
# Customer.aliases_sym('techsupport/repto') --> :a8
    jh = Customer.join_by_paths_helper('salesrep', 'techsupport/repto')
    act = Customer.get_alias_sym('techsupport/repto')
    as = Customer.get_alias_sym('salesrep')
    res = jh.dataset.where(Sequel[act][:FirstName] => 'Bill').order(Sequel[as][:BirthDate]).to_a

     # brute force check the returned set of CustomerId's
    exp_customer_ids = {}
    Employee.where(:FirstName => 'Bill').to_a.each{|leadr|
      Employee.where(:ReportsTo => leadr.EmployeeId).to_a.each{|techsupport|
        Customer.where(:TechSupportId => techsupport.EmployeeId).to_a.each{|customer|
          exp_customer_ids[customer.CustomerId ] = customer.salesrep.BirthDate
          }
      }
    }


    resh = {}
    res.each{|customer| resh[customer.CustomerId] = customer.salesrep.BirthDate}

    exp_customer_ids = exp_customer_ids.sort_by{|k,v| [v,k] }
    #check content as a set
    assert_equal exp_customer_ids.to_set, resh.to_a.to_set
    # check sorting
    assert_sequel_coll_ord_asc_by(res){|cust| cust.salesrep.BirthDate}
  end

end
