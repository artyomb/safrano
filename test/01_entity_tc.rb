
require_relative './test_saildb.rb' # Sail-DB testdata
require 'date'

class EntityTC < SailDBTest

# test access with a single PK id
  def test_race_model_single_pk
    get '/Race(1)'
    assert_last_resp_entity_parseable_ok('Race')

    # Race( 2) urlencoded space %20 should be ok
    get '/Race(%202)'
    assert_last_resp_entity_parseable_ok('Race')

    get '/Race(xyz99xxx)'
    assert_bad_request

    get '/Race(9999)'
    assert last_response.not_found?

  end

  def assert_json_attribute_val(exp_val, attr)
    assert last_response.ok?
    assert_equal 'application/json;charset=utf-8', last_response.content_type
    assert_nothing_raised do
      @resh = JSON.parse last_response.body
    end
    assert_equal(exp_val, @resh['d'][attr])
  end

  def assert_json_datetime_attribute_val(exp_val, attr)
    assert last_response.ok?
    assert_equal 'application/json;charset=utf-8', last_response.content_type
    assert_nothing_raised do
      @resh = JSON.parse last_response.body
    end
    assert_equal(exp_val.to_edm_json, @resh['d'][attr])
  end

  def assert_raw_val_attribute(exp_val)
    assert last_response.ok?
    assert_equal 'text/plain;charset=utf-8',
                 last_response.content_type

    assert_equal(exp_val, last_response.body)
  end


  def assert_raw_datetime_val_attribute(exp_val)
    assert last_response.ok?
    assert_equal 'text/plain;charset=utf-8', last_response.content_type

    assert_equal(exp_val.to_edm_json, last_response.body)
  end

# test access attribute with a single PK id
  def test_single_pk_attribute
    get '/Race(1)/name'
    assert_json_attribute_val(Race[1].name, 'name')

    get '/Race(1)/name/$value'
    assert_raw_val_attribute(Race[1].name)

    get '/Race(1)/xyzname'
    assert_not_found_error

    get '/Race(1)/namexyz'
    assert_not_found_error
  end

# test access attribute with a multi PK id
  def test_multi_pk_datetime_attribute
    get "/Edition(race_id=1,num=8)/start"
    assert_json_datetime_attribute_val(Edition[1,8].start, 'start')

    get "/Edition(race_id=1,num=8)/start/$value"
    assert_raw_datetime_val_attribute(Edition[1,8].start)

    # wrong key field name race_num instead of num --> Bad Request !
    get "/Edition(race_id=1,race_num=8)/xstart"
    assert_bad_request
    
    # wrong key field name race_num instead of num --> Bad Request !
    get "/Edition(race_id=1,race_num=8)/startx"
    assert_bad_request
    
    #  wrong attrib name xstart instead of start --> Not found !
    get "/Edition(race_id=1,num=8)/xstart"
    assert_not_found_error 'xstart', 'segment'
    
    #  wrong attrib name startx instead of start --> Not found !
    get "/Edition(race_id=1,num=8)/startx"
    assert_not_found_error 'startx', 'segment'

  end


# test access with a multiple PK
  def test_multi_pk

    get "/Ranking(race_id=1,race_num=8,boat_class=2,rank=1,exaequo=0)"
    assert_last_resp_entity_parseable_ok('Ranking')
# check result is correct
    assert_equal(8, @jresd[:crew])
# real spaces are forbidden in URI's. Ok this is handled by ruby URI parse
# somewhere
    assert_raise(URI::InvalidURIError){
      get "/Ranking(race_id=1,  race_num=8,  boat_class=2, rank=1,exaequo=0)"
    }
# but URL-encoded spaces ie. %20 should work !
    get "/Ranking(race_id=1,%20race_num=8,boat_class=2,%20rank=1,exaequo=0)"
    assert_last_resp_entity_parseable_ok('Ranking')
# check result is correct
    assert_equal(8, @jresd[:crew])
# should not work with quotes
    get "/Ranking(race_id=1,race_num='8',boat_class=2,rank=1,exaequo=0)"
    assert_bad_request
    
# check result is correct
    assert_equal(8, @jresd[:crew])
# check not found by providing a wrong exaequo
    get "/Ranking(race_id=1,race_num=8,boat_class=2,rank=1,exaequo=1)"
    assert last_response.not_found?
    

    get "/Ranking(race_id=3,race_num=11,boat_class=1,rank=1,exaequo=0)"
    assert_last_resp_entity_parseable_ok('Ranking')
# check result is correct
    assert_equal(1, @jresd[:crew])

# check an ex-aequo record
    get "/Ranking(race_id=1,race_num=6,boat_class=2,rank=3,exaequo=1)"
    assert_last_resp_entity_parseable_ok('Ranking')
# check result is correct
    assert_equal(28, @jresd[:crew])

    get "/Ranking(race_id=1,race_num=6,boat_class=2,rank=3,exaequo=2)"
    assert_last_resp_entity_parseable_ok('Ranking')
# check result is correct
    assert_equal(15, @jresd[:crew])

# check not found
    get "/Ranking(race_id=3,race_num=11,boat_class=1,rank=1,exaequo=1)"
    assert last_response.not_found?
  end

# test access with a multiple PK bad request
  def test_multi_pk_bad_req

    get "/Ranking(race_id='1a',race_num='8',boat_class='2',rank='1',exaequo='0')"
    assert_bad_request

    get "/Ranking(race_id='1',race_num='8',boat_class='2',rank='1',exaequo='X')"
    assert_bad_request

  end
  
  # multi pk key predicate order 
  def test_multi_pk_predicate_order
    get "/Edition(num=8,race_id=1)"
    assert_last_resp_entity_parseable_ok('Edition')
    assert_sequel_values_equal(Edition[1,8], @jresd, 
                               uri: 'Edition(race_id=1,num=8)')

    get "/Edition(race_id=3,num=11)"
    assert_last_resp_entity_parseable_ok('Edition')
    assert_sequel_values_equal(Edition[3,11], @jresd, 
                               uri: 'Edition(race_id=3,num=11)')

    get "/Edition(num=666,race_id=666)"
    assert last_response.not_found?
  end
end
