

require 'test/unit/testsuite'
require 'sequel'
require_relative  'test_helper.rb'

class TSWithoutDB
  def self.suite
    suites = Test::Unit::TestSuite.new("TSWithoutDB")
    suites <<  TCWithoutDB.subsuites
    suites
  end
end

class TSWithDB

  def self.suite
    suites = Test::Unit::TestSuite.new("TSWithDB")
    suites << SailDBTest.subsuites
    suites << SailDBTestII.subsuites
    suites << ChinookDBTest.subsuites
    suites << ChinookDBTestII.subsuites
    suites << BotanicsRackTest.subsuites
    suites << BotanicsDBTest.subsuites
    suites << BotypesRackTest.subsuites
    suites << BotypesDBTest.subsuites
    suites << BotypesRackTestII.subsuites
    suites
  end
end

