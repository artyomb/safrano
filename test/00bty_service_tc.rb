
require_relative './test_botypes_db.rb' # Botypes-DB testdata

class Metadata_Types_TC < BotypesRackTest
  def setup
    # we will just test different parts of the XML result
    # in each test
    
    get '/$metadata'
    assert_equal "application/xml;charset=utf-8", 
    last_response.content_type

    assert last_response.ok?

    @xml = REXML::Document.new(last_response.body)
    
    assert_equal('http://schemas.microsoft.com/ado/2007/06/edmx',
                 @xml.root.attributes['xmlns:edmx'])
                 
    assert_equal('Edmx', @xml.root.name)
    assert_equal('edmx', @xml.root.prefix)
    
    @xmlds = @xml.root.elements.first
    assert_equal 'DataServices', @xmlds.name
    
    @xmlsch = @xmlds.elements.first
    assert_equal 'Schema', @xmlsch.name
    
    
    assert @ec = @xmlsch.elements.find('EntityContainer'){|e| 
      e.attributes['Name'] == 'TypeService'
    }
  end
# more complete testing for Numeric fields taht were not exported (bug in version 0.5.2/0.5.3)
# (should be Edm.Decimal in Odata V2)
  def test_it_has_metadata_Numeric

     
    assert entyTypeXml = @xmlsch.elements.find('EntityType'){|e| 
      e.attributes['Name'] == 'DBTypeNum'
    }

    entyProps = { id: 'Edm.Int32',
                      nume: 'Edm.Decimal',
                      deci: 'Edm.Decimal',
                      nume2: 'Edm.Decimal(2)',
                      deci2: 'Edm.Decimal(2)',
                      nume5_2: 'Edm.Decimal(5,2)',
                      deci5_2: 'Edm.Decimal(5,2)',
                      nume16_2: 'Edm.Decimal(16,2)',
                      tfield: 'Edm.String',
                      expe_str: 'Edm.String'                      }
    
    entyProps.each{|name, type| 
      a = entyTypeXml.each_element('Property'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
            (   e.attributes['Type'] == type ) )
      }
      unless a
        require 'pry'
        binding.pry
      end
      assert a, "#{name}, #{type} not ok"

    }
    
     
  end  
  
  def test_it_has_metadata_diverse_types
      
    assert entyTypeXml = @xmlsch.elements.find('EntityType'){|e| 
      e.attributes['Name'] == 'DBTypeFloat'
    }

    entyProps = { id: 'Edm.Int32',
                  realn: 'Edm.Double',
                  doublen: 'Edm.Double',
                  floatn: 'Edm.Double',
                  float2: 'Edm.Double',
                  smallint: 'Edm.Int16',
                  int2: 'Edm.Int16',
                  integer: 'Edm.Int32',
                  int4: 'Edm.Int32',
                  int: 'Edm.Int32',
                  int8: 'Edm.Int64',
                  bigint: 'Edm.Int64',
                  boolean: 'Edm.Boolean',
                  binary: 'Edm.Binary',
                  blob: 'Edm.Binary'
                  }
    
    entyProps[:mediumint] = 'Edm.Int32' if  Sequel::Model.db.adapter_scheme == :sqlite
    entyProps[:byte] = 'Edm.Byte' if  Sequel::Model.db.adapter_scheme == :sqlite
    
    entyProps.each{|name, type| 
      assert entyTypeXml.each_element('Property'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
            (   e.attributes['Type'] == type ) )
      }, "#{name}, #{type} not ok"

    }
    
     
  end  
    
  # Test with default settings
  def test_datetime_types_default
      
    assert entyTypeXml = @xmlsch.elements.find('EntityType'){|e| 
      e.attributes['Name'] == 'DBTypeDateTime'
    }

    entyProps = { id: 'Edm.Int32',
                  date: 'Edm.DateTime',
                  datetime: 'Edm.DateTime',
                  datetimetz: 'Edm.DateTime',
                  comment: 'Edm.String'
                  }
    
    entyProps.each{|name, type| 
      if type == 'Edm.DateTime'
        assert entyTypeXml.each_element('Property'){}.find{|e| 
              ( ( e.attributes['Name'] == name.to_s )  and 
              # for Edm.DateTime we only support Precision 0
              (   e.attributes['Precision'] == '0' ) and 
              (   e.attributes['Type'] == type ) )
        }, "#{name}, #{type} not ok"
      else
        assert entyTypeXml.each_element('Property'){}.find{|e| 
              ( ( e.attributes['Name'] == name.to_s )  and 
              (   e.attributes['Type'] == type ) )
        }, "#{name}, #{type} not ok"     
      end
    }
    
     
  end    
  
  # Test with default settings
  def test_guid_type
      
    assert entyTypeXml = @xmlsch.elements.find('EntityType'){|e| 
      e.attributes['Name'] == 'DBTypeGuid'
    }

    entyProps = { guid: 'Edm.Guid',
                  guid_str: 'Edm.String',
                  comment: 'Edm.String'
                  }
    
    entyProps[:guid] = 'Edm.Binary' if  Sequel::Model.db.adapter_scheme == :sqlite

    
    entyProps.each{|name, type| 
      assert entyTypeXml.each_element('Property'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
            (   e.attributes['Type'] == type ) )
      }, "#{name}, #{type} not ok"

    }
    
     
  end      
end
# for testing Safrano's type settings overriding functionality 
class MetadataTypesOverrideíng_TC < BotypesRackTestII
  def setup
    # we will just test different parts of the XML result
    # in each test
    
    get '/$metadata'
    assert_equal "application/xml;charset=utf-8", 
    last_response.content_type

    assert last_response.ok?

    @xml = REXML::Document.new(last_response.body)
    
    assert_equal('http://schemas.microsoft.com/ado/2007/06/edmx',
                 @xml.root.attributes['xmlns:edmx'])
                 
    assert_equal('Edmx', @xml.root.name)
    assert_equal('edmx', @xml.root.prefix)
    
    @xmlds = @xml.root.elements.first
    assert_equal 'DataServices', @xmlds.name
    
    @xmlsch = @xmlds.elements.first
    assert_equal 'Schema', @xmlsch.name
    
    
    assert @ec = @xmlsch.elements.find('EntityContainer'){|e| 
      e.attributes['Name'] == 'TypeService'
    }
  end
  
  # Test with explicitely specified type (especially for SQlite/Blob combination)
  def test_guid_type
      
    assert entyTypeXml = @xmlsch.elements.find('EntityType'){|e| 
      e.attributes['Name'] == 'DBTypeGuidII'
    }

    entyProps = { guid: 'Edm.Guid',
                  guid_str: 'Edm.String',
                  comment: 'Edm.String'  }
    
    entyProps.each{|name, type| 
      assert entyTypeXml.each_element('Property'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
            (   e.attributes['Type'] == type ) )
      }, "#{name}, #{type} not ok"

    }
    
     
  end      
end
