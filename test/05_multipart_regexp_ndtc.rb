#!/usr/bin/env ruby

# here we test some gory regexp corner cases

require 'test/unit'
require_relative '../lib/safrano/multipart.rb'

module OData
  module Batch
    class Test_Http_Verb_Uri_Regexp < TCWithoutDB
      @@r = MIME::Content::Application::Http::HTTP_R_RGX

      def test_verb_uri_proto
        str = 'GET http://localhost:9595/Race(1) HTTP/1.1'

        md = str.match(@@r)
        assert_not_nil md, "HTTP_VERB_URI_REGEXP  failed matching #{str}"
        assert_equal 'GET', md[1]
        assert_equal 'http://localhost:9595/Race(1)', md[2]
        assert_equal 'HTTP/1.1', md[3]

        str = 'PATCH /Race(1) HTTP/1.1'

        md = str.match(@@r)
        assert_not_nil md, "HTTP_VERB_URI_REGEXP  failed matching #{str}"
        assert_equal 'PATCH', md[1]
        assert_equal '/Race(1)', md[2]
        assert_equal 'HTTP/1.1', md[3]
      end
    end
    class Test_Header_Regexp < TCWithoutDB

      @@r = MIME::Content::Application::Http::HEADER_RGX
      def test_key_value
        str = 'Content-Type: application/http'

        md = str.match(@@r)
        assert_not_nil md, "Header_Regexp failed matching #{str}"
        assert_equal 'Content-Type', md[1]
        assert_equal 'application/http', md[2]

        str = 'Content-Transfer-Encoding:binary'
        md = str.match(@@r)
        assert_not_nil md, "Header_Regexp failed matching #{str}"
        assert_equal 'Content-Transfer-Encoding', md[1]
        assert_equal 'binary', md[2]

        str = 'Content-Length:666'
        md = str.match(@@r)
        assert_not_nil md, "Header_Regexp failed matching #{str}"
        assert_equal 'Content-Length', md[1]
        assert_equal '666', md[2]
      end

      def test_key_value_param
        str = 'Accept: application/json;odata.metadata=minimal'

        md = str.match(@@r)
        assert_not_nil md, "Header_Regexp failed matching #{str}"
        assert_equal 'Accept', md[1]
        assert_equal 'application/json;odata.metadata=minimal', md[2]

        str = 'Content-Type: multipart/mixed;boundary=batch_36522ad7-fc75-4b56-8c71-56071383e77b'
        md = str.match(@@r)
        assert_not_nil md, "Header_Regexp failed matching #{str}"
        assert_equal 'Content-Type', md[1]
        assert_equal 'multipart/mixed;boundary=batch_36522ad7-fc75-4b56-8c71-56071383e77b', md[2]
      end
    end
  end
end
