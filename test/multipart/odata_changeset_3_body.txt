
--batch_48f8-3aea-2f04
Content-Type: multipart/mixed; boundary=changeset_fa99-0523-ad8a

--changeset_fa99-0523-ad8a
Content-Type: application/http
Content-Transfer-Encoding: binary

MERGE Race(14) HTTP/1.1
Accept: application/json
Content-Type: application/json

{"__metadata":{"uri":"http://example.org/Race(14)","type":"Race"},"periodicity":"3", "crew_type":"2"}

--changeset_fa99-0523-ad8a
Content-Type: application/http
Content-Transfer-Encoding: binary

MERGE Race(15) HTTP/1.1
Accept: application/json
Content-Type: application/json

{"__metadata":{"uri":"http://example.org/Race(15)","type":"Race"},"crew_type":"10"}

--changeset_fa99-0523-ad8a--

--batch_48f8-3aea-2f04
Content-Type: application/http
Content-Transfer-Encoding: binary

GET Race(14) HTTP/1.1
Accept: application/json

--batch_48f8-3aea-2f04--
