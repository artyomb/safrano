
require_relative './test_botypes_db.rb' # Botypes-DB testdata
require 'base64'

# for testing Safrano's type settings overriding functionality 
class FieldTypesOverrideíng_TC < BotypesRackTestII

# from model.rb
    #with_db_type(/numeric|decimal/) do 
      #edm_type 'Edm.Decimal'
      #json_value ->(val){  "Numeric|Decimal param-free overriding #{val}" }
      
      #with_one_param{|param| 
        #edm_type "Edm.Decimal(#{param})"
        #json_value ->(val, parm){  "Numeric|Decimal one-param(#{param}) overriding #{val}"  }
      #}
      
      #with_two_params{|par1, par2| 
        #edm_type "Edm.Decimal(#{par1},#{par2})"
        #json_value ->(val, p1, p2){  "Numeric|Decimal two-param(#{par1}, #{par2}) overriding #{val}"  }
      #}
            
    ##end
 #-- Postgresql follows SQL standard setting default scale to 0, ie. integers !
#-- inserting 3.1 ends up into 3 anyway, thus here we test with ints
#INSERT INTO "dbtypenum" ( "id", "nume2", "tfield", "expe_str" ) VALUES (3, 13, 'nume2',  '13.0');
#INSERT INTO "dbtypenum" ( "id", "deci2", "tfield", "expe_str" ) VALUES (4, 13, 'deci2',  '13.0');
#INSERT INTO "dbtypenum" ( "id", "nume5_2", "tfield", "expe_str" ) VALUES (5, 312.11,  'nume5_2', '312.11');
#INSERT INTO "dbtypenum" ( "id", "nume5_2", "tfield", "expe_str" ) VALUES (6, 312.10, 'nume5_2',  '312.10');
#INSERT INTO "dbtypenum" ( "id", "nume5_2", "tfield", "expe_str" ) VALUES (7, 12.11, 'nume5_2',  '12.11');
#INSERT INTO "dbtypenum" ( "id", "nume5_2", "tfield", "expe_str" ) VALUES (8, 12.10, 'nume5_2',  '12.10');
#INSERT INTO "dbtypenum" ( "id", "nume5_2", "tfield", "expe_str" ) VALUES (9, 12.1, 'nume5_2',  '12.10');
#INSERT INTO "dbtypenum" ( "id", "deci5_2", "tfield", "expe_str" ) VALUES (10, 312.11, 'deci5_2',  '312.11');
#INSERT INTO "dbtypenum" ( "id", "deci5_2", "tfield", "expe_str" ) VALUES (11, 312.10, 'deci5_2',  '312.10');
#INSERT INTO "dbtypenum" ( "id", "deci5_2", "tfield", "expe_str" ) VALUES (12, 12.11, 'deci5_2',  '12.11');
#INSERT INTO "dbtypenum" ( "id", "deci5_2", "tfield", "expe_str" ) VALUES (13, 12.10, 'deci5_2',  '12.10');
#INSERT INTO "dbtypenum" ( "id", "deci5_2", "tfield", "expe_str" ) VALUES (14, 12.1, 'deci5_2',  '12.10');
#INSERT INTO "dbtypenum" ( "id", "nume16_2", "tfield", "expe_str" ) VALUES (15, 12022.1, 'nume16_2','12022.10');
#INSERT INTO "dbtypenum" ( "id", "nume16_2", "tfield", "expe_str" ) VALUES (16, 12022555555555.5, 'nume16_2','12022555555555.50');
#INSERT INTO "dbtypenum" ( "id", "nume16_2", "tfield", "expe_str" ) VALUES (17, 1202255555555.55, 'nume16_2','1202255555555.55');
  def test_guid_type
    get "/DBTypeGuidII(guid'43ae7f2a-f38c-4d16-88bc-4b2149725357')"

    assert_last_resp_enty_ok_no_cast(DBTypeGuidII)

    renv = last_request.env
    expected = "#{renv['rack.url_scheme']}://#{renv['SERVER_NAME']}/DBTypeGuidII(guid'43ae7f2a-f38c-4d16-88bc-4b2149725357')"
    assert_equal expected, @jrawd[:__metadata][:uri]
    
    assert_equal '43ae7f2a-f38c-4d16-88bc-4b2149725357', @jrawd[:guid]
  end
    
  def test_decimal_numeric_types
    
    get "/DBTypeNumII(1)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal param-free overriding 0.31415e1', @jrawd[:nume]

    get "/DBTypeNumII(2)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal param-free overriding 0.31415e1', @jrawd[:deci]

    get "/DBTypeNumII(3)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    if  Sequel::Model.db.adapter_scheme == :postgres
      assert_equal  "Numeric|Decimal two-param(2, 0) overriding 0.3e1", @jrawd[:nume2]
    else
      assert_equal 'Numeric|Decimal one-param(2) overriding 0.31e1', @jrawd[:nume2]
    end
    
    get "/DBTypeNumII(4)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    if  Sequel::Model.db.adapter_scheme == :postgres
      assert_equal  "Numeric|Decimal two-param(2, 0) overriding 0.3e1", @jrawd[:deci2]
    else
      assert_equal 'Numeric|Decimal one-param(2) overriding 0.31e1', @jrawd[:deci2]
    end


    get "/DBTypeNumII(5)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(5, 2) overriding 0.31211e3', @jrawd[:nume5_2]

    get "/DBTypeNumII(6)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(5, 2) overriding 0.3121e3', @jrawd[:nume5_2]

    get "/DBTypeNumII(7)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(5, 2) overriding 0.1211e2', @jrawd[:nume5_2]

    get "/DBTypeNumII(8)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(5, 2) overriding 0.121e2', @jrawd[:nume5_2]

    get "/DBTypeNumII(9)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(5, 2) overriding 0.121e2', @jrawd[:nume5_2]


    get "/DBTypeNumII(10)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(5, 2) overriding 0.31211e3', @jrawd[:deci5_2]

    get "/DBTypeNumII(11)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(5, 2) overriding 0.3121e3', @jrawd[:deci5_2]

    get "/DBTypeNumII(12)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(5, 2) overriding 0.1211e2', @jrawd[:deci5_2]

    get "/DBTypeNumII(13)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(5, 2) overriding 0.121e2', @jrawd[:deci5_2]

    get "/DBTypeNumII(14)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(5, 2) overriding 0.121e2', @jrawd[:deci5_2]


    get "/DBTypeNumII(15)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(16, 2) overriding 0.120221e5', @jrawd[:nume16_2]

    get "/DBTypeNumII(16)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(16, 2) overriding 0.120225555555555e14', @jrawd[:nume16_2]

    get "/DBTypeNumII(17)"
    assert_last_resp_enty_ok_no_cast(DBTypeNumII)
    assert_equal 'Numeric|Decimal two-param(16, 2) overriding 0.120225555555555e13', @jrawd[:nume16_2]

    
  end

  def test_float_and_int_like_values
    # actually not what OData expects (byte-1 int) lets not support this yet
    
    if  Sequel::Model.db.adapter_scheme == :postgres
      # in 0.5.5 now this works too thanks to
      get "/DBTypeFloatII(15)"
      assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
      assert_equal 120, @jrawd[:byte]    # x is ascii 120
    end    
    
    get "/DBTypeFloatII(1)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 1.12346, @jrawd[:realn]
    
    get "/DBTypeFloatII(2)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 1.12345678912346, @jrawd[:doublen]
    
    get "/DBTypeFloatII(3)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 1.12346, @jrawd[:floatn]
    
    get "/DBTypeFloatII(4)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 1.12346, @jrawd[:float2]
    
    get "/DBTypeFloatII(5)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 1.12345678912346, @jrawd[:float30]
    
    get "/DBTypeFloatII(6)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 32000, @jrawd[:smallint]
    
    get "/DBTypeFloatII(7)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 32000, @jrawd[:int2]
    
    get "/DBTypeFloatII(8)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 2147483647, @jrawd[:integer]
    
    get "/DBTypeFloatII(9)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 2147483647, @jrawd[:int4]
    
    get "/DBTypeFloatII(10)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 2147483647, @jrawd[:int]    

    
    get "/DBTypeFloatII(11)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 9223372036854775806, @jrawd[:int8]
    
    get "/DBTypeFloatII(12)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 9223372036854775806, @jrawd[:bigint]      
    
    if  Sequel::Model.db.adapter_scheme == :sqlite
      get "/DBTypeFloatII(13)"
      assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
      assert_equal 4294967290, @jrawd[:mediumint]
      
      get "/DBTypeFloatII(15)"
      assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
      assert_equal 120, @jrawd[:byte] 
    end
    
   
    get "/DBTypeFloatII(16)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal false, @jrawd[:boolean]   
      
    get "/DBTypeFloatII(17)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal true, @jrawd[:boolean]     

    # OData json representation of binary is  base64 encoded string
    get "/DBTypeFloatII(18)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
    assert_equal 'oz', Base64.decode64( @jrawd[:blob])   

     
    # ok doesnt work, we get it back as a Float ?
    # --> remove it from supported list of int variants
    
    #if  Sequel::Model.db.adapter_scheme == :sqlite
      #get "/DBTypeFloatII(14)"
      #assert_last_resp_enty_ok_no_cast(DBTypeFloatII)
      #assert_equal 18446744073709551614, @jrawd[:ubigint]  
    #end

  end

end

# for testing Safrano's Standard types settings
class FieldTypes_TC < BotypesRackTest

  def test_decimal_numeric_types
    l = DBTypeNum.all.to_a.dup
    l.each{|t| 
      get "DBTypeNum(#{t.id})" 

      assert_last_resp_enty_ok_no_cast(DBTypeNum)
      assert_equal t.expe_str, @jrawd[t.tfield.to_sym]
    }
  end
  
#INSERT INTO `dbtypefloat` ( `id`, `realn` ) VALUES (1, 1.12346);
#INSERT INTO `dbtypefloat` ( `id`, `doublen` ) VALUES (2, 1.12345678912346);
#INSERT INTO `dbtypefloat` ( `id`, `floatn` ) VALUES (3, 1.12346);
#INSERT INTO `dbtypefloat` ( `id`, `float2` ) VALUES (4, 1.12346);
#INSERT INTO `dbtypefloat` ( `id`, `float30` ) VALUES (5, 1.12345678912346);
#INSERT INTO `dbtypefloat` ( `id`, `smallint` ) VALUES (6, 32000);
#INSERT INTO `dbtypefloat` ( `id`, `int2` ) VALUES (7, 32000);
#INSERT INTO `dbtypefloat` ( `id`, `integer` ) VALUES (8, 2147483647);
#INSERT INTO `dbtypefloat` ( `id`, `int4` ) VALUES (9, 2147483647);
#INSERT INTO `dbtypefloat` ( `id`, `int` ) VALUES (10, 2147483647);
#INSERT INTO `dbtypefloat` ( `id`, `mediumint` ) VALUES (11, 2147483647);
#INSERT INTO `dbtypefloat` ( `id`, `int8` ) VALUES (12, 9223372036854775806);
#INSERT INTO `dbtypefloat` ( `id`, `bigint` ) VALUES (13, 9223372036854775806);  
  
  def test_float_and_int_like_values
    get "/DBTypeFloat(1)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 1.12346, @jrawd[:realn]
    
    get "/DBTypeFloat(2)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 1.12345678912346, @jrawd[:doublen]
    
    get "/DBTypeFloat(3)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 1.12346, @jrawd[:floatn]
    
    get "/DBTypeFloat(4)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 1.12346, @jrawd[:float2]
    
    get "/DBTypeFloat(5)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 1.12345678912346, @jrawd[:float30]
    
    get "/DBTypeFloat(6)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 32000, @jrawd[:smallint]
    
    get "/DBTypeFloat(7)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 32000, @jrawd[:int2]
    
    get "/DBTypeFloat(8)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 2147483647, @jrawd[:integer]
    
    get "/DBTypeFloat(9)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 2147483647, @jrawd[:int4]
    
    get "/DBTypeFloat(10)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 2147483647, @jrawd[:int]    

    
    get "/DBTypeFloat(11)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 9223372036854775806, @jrawd[:int8]
    
    get "/DBTypeFloat(12)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 9223372036854775806, @jrawd[:bigint]      
    
    if  Sequel::Model.db.adapter_scheme == :sqlite
      get "/DBTypeFloat(13)"
      assert_last_resp_enty_ok_no_cast(DBTypeFloat)
      assert_equal 4294967290, @jrawd[:mediumint]
      
      get "/DBTypeFloat(15)"
      assert_last_resp_enty_ok_no_cast(DBTypeFloat)
      assert_equal 120, @jrawd[:byte] 
    end
    
   
    get "/DBTypeFloat(16)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal false, @jrawd[:boolean]   
      
    get "/DBTypeFloat(17)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal true, @jrawd[:boolean]     

    # OData json representation of binary is  base64 encoded string
    get "/DBTypeFloat(18)"
    assert_last_resp_enty_ok_no_cast(DBTypeFloat)
    assert_equal 'oz', Base64.decode64( @jrawd[:blob])   

     
    # ok doesnt work, we get it back as a Float ?
    # --> remove it from supported list of int variants
    
    #if  Sequel::Model.db.adapter_scheme == :sqlite
      #get "/DBTypeFloat(14)"
      #assert_last_resp_enty_ok_no_cast(DBTypeFloat)
      #assert_equal 18446744073709551614, @jrawd[:ubigint]  
    #end
    
  end
   
 
  
end
