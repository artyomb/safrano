
# see https://www.rubydoc.info/gems/sequel/4.38.0/Sequel/Timezones


create_dummy_tables('botypes')

class DBTypeNum  < Sequel::Model(:dbtypenum)
end
class DBTypeFloat  < Sequel::Model(:dbtypefloat)
end
class DBTypeDateTime  < Sequel::Model(:dbtypedatetime)
end
class DBTypeGuid  < Sequel::Model(:dbtypeguid)
end
# create a few test records
create_dummy_data('botypes')

### SERVER Part #############################
include Safrano::Edm
 
# for testing Safrano's Standard types settings
class ODataBotypesApp < Safrano::ServerApp
  publish_service do

    title  'Type testing OData API'
    name  'TypeService'
    namespace  'ODataTy'
    server_url 'http://example.org'
    path_prefix '/'

#    bugfix_create_response true
    
    # lets test user-defined type mapping with
    # postgresql's internal  1-byte "char" type, which is what comes closest to a byte type in pg
    # but OData expects it as a 1-byte int. Thus the convertion is with the String.ord (1-char ordinal)
    with_db_type('"char"') do 
      edm_type 'Edm.Byte'
      json_value ->(val){  val&.ord }
    end
    
    publish_model DBTypeNum
    publish_model DBTypeFloat
    publish_model DBTypeDateTime 
    publish_model DBTypeGuid
        
  end
    
end

# for testing Safrano's type settings overriding functionality 

class DBTypeNumII  < Sequel::Model(:dbtypenum)
end
class DBTypeFloatII  < Sequel::Model(:dbtypefloat)
end
class DBTypeDateTimeII  < Sequel::Model(:dbtypedatetime)
end
class DBTypeGuidII  < Sequel::Model(:dbtypeguid)
end
def decimal_with_precision_scale_output(val, precision, scale)  
  "Numeric|Decimal two-param(#{precision}, #{scale}) overriding #{val}"  
end

class ODataBotypesAppII < Safrano::ServerApp
  publish_service do

    title  'Type testing OData API'
    name  'TypeService'
    namespace  'ODataTyII'
    server_url 'http://example.org'
    path_prefix '/'

    # lets test user-defined type mapping with
    # postgresql's internal  1-byte "char" type, which is what comes closest to a byte type in pg
    # but OData expects it as a 1-byte int. Thus the convertion is with the String.ord (1-char ordinal)
    with_db_type('"char"') do 
      edm_type 'Edm.Byte'
      json_value ->(val){  val&.ord }
    end
    
    with_db_type('numeric', 'decimal') do 
      edm_type 'Edm.Decimal'
      json_value ->(val){  "Numeric|Decimal param-free overriding #{val}" }
      
      with_one_param{|param| 
        edm_type "Edm.Decimal(#{param})"
        json_value ->(val){ "Numeric|Decimal one-param(#{param}) overriding #{val}"  }
      }
      
      with_two_params{|precision, scale| 
        edm_type "Edm.Decimal(#{precision},#{scale})"
        json_value ->(val){  decimal_with_precision_scale_output(val, precision, scale) }
      }
            
    end
 
  
    publish_model DBTypeNumII
    publish_model DBTypeFloatII
    publish_model DBTypeDateTimeII 
    
    # Sqlite does not have a native UUID type, so we store guid as 16-byte blob; but
    # we have to explicitely say it is a Guid here.
    # With this combination db-type = blob and explicit Edm type Guid, Safrano has built-in convertions
    if Sequel::Model.db.adapter_scheme == :sqlite
      publish_model DBTypeGuidII  do 
        with_attribute(:guid) { edm_type 'Edm.Guid' }
      end
    else # postgresql has native UUID type. This mapped automatically to Edm.Guid
      publish_model DBTypeGuidII 
    end     
  end
    
end