#!/usr/bin/env ruby
require 'sequel'
require 'tzinfo'
Sequel.extension :named_timezones
Sequel.default_timezone = :utc
Sequel.application_timezone = nil
Sequel.typecast_timezone =   'Europe/Berlin'
Sequel.datetime_class = DateTime
begin
    db = Sequel.sqlite('./botypes_v0.db3') 
    class DBTypeDateTime  < Sequel::Model(:dbtypedatetime)
    end
    # Attempt some action that may have an exception in SQLite 
    p DBTypeDateTime[5]
    p DBTypeDateTime[6] 
    p DBTypeDateTime[5].datetimetz
    p DBTypeDateTime[6].datetimetz
    p DBTypeDateTime[5].datetimetz.class
    p DBTypeDateTime[6].datetimetz.class
rescue SQLite3::Exception => e 
    # Handle the exception gracefully
ensure
    # If the whole application is going to exit and you don't
    # need the database at all any more, ensure db is closed.
    # Otherwise database closing might be handled elsewhere.
    at_exit { Sequel::Model.db.disconnect if Sequel::Model.db }
end


