BEGIN TRANSACTION;
SET CONSTRAINTS ALL DEFERRED;
DROP TABLE IF EXISTS "dbtypenum" CASCADE;
CREATE TABLE IF NOT EXISTS "dbtypenum" (
	"id"	SERIAL NOT NULL PRIMARY KEY,
	"nume"	NUMERIC,
  "deci"	DECIMAL,
	"nume2"	NUMERIC(2),
  "deci2"	DECIMAL(2),
	"nume5_2"	NUMERIC(5,2),
  "deci5_2"	DECIMAL(5,2),
  "nume16_2"	NUMERIC(16,2),
  "tfield" TEXT,
  "expe_str" TEXT
);
DROP TABLE IF EXISTS "dbtypefloat" CASCADE;
CREATE TABLE IF NOT EXISTS "dbtypefloat" (
	"id"	SERIAL NOT NULL PRIMARY KEY ,
  "realn" REAL,
  "doublen" double precision,
  "floatn" FLOAT,
  "float2" FLOAT(2),
  "float30" FLOAT(30) ,
  "smallint" SMALLINT,
  "int2" INT2,
  "integer" INTEGER,
  "int4" INT4,
  "int" INT,
  "int8" INT8,
  "bigint" BIGINT  ,
  "boolean" boolean,
  "binary" bytea,
-- cf https://www.postgresql.org/docs/9.1/datatype-character.html
-- internal type "char" (quoted and different from char(1) ) is 1-byte        
        "byte" "char",
        "blob" bytea
--  "mediumint" MEDIUMINT,       
--  "ubigint" unsigned big int       
);

DROP TABLE IF EXISTS "dbtypedatetime";
CREATE TABLE IF NOT EXISTS "dbtypedatetime" (
	"id"	SERIAL NOT NULL PRIMARY KEY,
	"date"	DATE,
	"datetime"	timestamp without time zone ,
  "datetimetz"	timestamp with time zone ,
	"iso8601_utc"	TEXT,
	"iso8601"	TEXT,
	"edm_json_utc"	TEXT,
	"edm_json"	TEXT,
	"comment"	TEXT
);
ALTER TABLE public."dbtypedatetime" OWNER TO safrano;

 DROP TABLE IF EXISTS "dbtypeguid";
 CREATE TABLE "dbtypeguid" (
	"guid"	UUID NOT NULL,
  "guid_str" VARCHAR,
	"comment"	VARCHAR,
	PRIMARY KEY("guid")
);


COMMIT;
