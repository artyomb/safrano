$LOAD_PATH << '../../lib/'
require_relative '../../lib/safrano'

require_relative  '../run_helper'
require 'thin'

require 'sequel'

  $DB = {}

dbrname = File.join(__dir__, 'botypes_v0.db3')
$DB[:botypes] = Sequel::Model.db = Sequel.sqlite(dbrname)


at_exit {
 Sequel::Model.db.disconnect if Sequel::Model.db 
 }

require_relative './model.rb'
