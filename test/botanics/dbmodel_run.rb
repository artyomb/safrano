$LOAD_PATH << '../../lib/'
require_relative '../../lib/safrano'


require_relative  '../run_helper'

require 'sequel'

  $DB = {}

dbrname = File.join(__dir__, 'botanics_test.db3')
$DB[:botanics] = Sequel::Model.db = Sequel.sqlite(dbrname)


at_exit {
 Sequel::Model.db.disconnect if Sequel::Model.db 

}

require_relative './model.rb'
