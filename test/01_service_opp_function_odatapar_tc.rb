
require_relative './test_botanics_db.rb' # Botanics-DB testdata

# function returning entities or collection of entities shall
# support OData parameters like $top,$skip,$expand
class ServiceOppFunction_ODataParam_TC < BotanicsRackTest


  # expand entity works?
  # first_cultivar_func --> no expand handling
  # first_cultivar_func_qpar -->  expand handling
  def test_entity_func_expand
    get "first_cultivar_func_qpar?$expand=photo,breeder,institute"
    assert_last_resp_entity_parseable_ok('Cultivar')
    assert_expanded_entity(Cultivar.first, @jresd, :institute){|expanded| "Institute(#{expanded.id})"}
    assert_expanded_entity(Cultivar.first, @jresd, :breeder){|expanded| "Breeder(#{expanded.id})"}
# this assert does not work yet because it's a media entity attribute    
#    assert_expanded_entity_mult(Cultivar.first, @jresd, :photo){|expanded| "Photo(#{expanded.id})"}

  end
  
  # default is NO expand entity, for funcs that do not call .apply_query_parameters like first_cultivar_func
  # first_cultivar_func --> no expand handling
  # first_cultivar_func_qpar -->  expand handling
  def test_entity_func_NO_expand
    get "first_cultivar_func?$expand=photo,breeder,institute"
    assert_last_resp_entity_parseable_ok('Cultivar')
    assert_not_expanded_entity(Cultivar.first, @jresd, :institute)
    assert_not_expanded_entity(Cultivar.first, @jresd, :breeder)
# this assert does not work yet because it's a media entity attribute    
#    assert_expanded_entity_mult(Cultivar.first, @jresd, :photo){|expanded| "Photo(#{expanded.id})"}

  end
  
  # expand entity collection works?
  def test_ecoll_func_expand
    get "/top10_cultivar_func_qpar?$expand=photo,breeder,institute"
    expected = Cultivar.limit(10)
    assert_last_resp_coll_parseable_ok('Cultivar', expected.count)
    expected.all.each_with_index{|exp,idx|
      assert_expanded_entity(exp, @jresd_coll[idx], :breeder){|expanded| "Breeder(#{expanded.id})"}
      assert_expanded_entity(exp, @jresd_coll[idx], :institute){|expanded| "Institute(#{expanded.id})"}
    }
    
  end

  # $skip entity collection works?
  def test_ecoll_func_skip
    get "/top10_cultivar_func_qpar?$skip=3"
#    expected = Cultivar.offset(3).limit(7)
# no we dont get 7, we still get 10...maybe a bit counterintuitive but ok
    expected = Cultivar.offset(3).limit(10)
    assert_last_resp_coll_parseable_ok('Cultivar', expected.count)
    expected.all.each_with_index{|exp,idx|
      assert_not_expanded_entity(exp, @jresd_coll[idx], :breeder)
      assert_not_expanded_entity(exp, @jresd_coll[idx], :institute)
    }
    
  end

  # $top entity collection works?
  def test_ecoll_func_top
    get "/top10_cultivar_func_qpar?$top=3"

    expected = Cultivar.limit(3)
    assert_last_resp_coll_parseable_ok('Cultivar', expected.count)
    expected.all.each_with_index{|exp,idx|
      assert_not_expanded_entity(exp, @jresd_coll[idx], :breeder)
      assert_not_expanded_entity(exp, @jresd_coll[idx], :institute)
    }
    
  end

  # $skip + $top entity collection works?
  def test_ecoll_func_skip_top
    get "/top10_cultivar_func_qpar?$skip=1&$top=3"

     expected = Cultivar.offset(1).limit(3)
     
    assert_last_resp_coll_parseable_ok('Cultivar', expected.count)
    expected.all.each_with_index{|exp,idx|
      assert_not_expanded_entity(exp, @jresd_coll[idx], :breeder)
      assert_not_expanded_entity(exp, @jresd_coll[idx], :institute)
    }
  end
  
  # $skip + $top + expand entity collection works?
  def test_ecoll_func_skip_top_expand
    get "/top10_cultivar_func_qpar?$skip=3&$top=2&$expand=photo,breeder,institute"

     expected = Cultivar.offset(3).limit(2)
     
    assert_last_resp_coll_parseable_ok('Cultivar', expected.count)
    expected.all.each_with_index{|exp,idx|
      assert_expanded_entity(exp, @jresd_coll[idx], :breeder){|expanded| "Breeder(#{expanded.id})"}
      assert_expanded_entity(exp, @jresd_coll[idx], :institute){|expanded| "Institute(#{expanded.id})"}

    }    
    
  end

  # finally test that it's not applied per default, with func top10_cultivar_func that has the 
  # query params option unset
  def test_ecoll_func_NOPAR_skip_top_expand
    get "/top10_cultivar_func?$skip=3&$top=2&$expand=photo,breeder,institute"

     expected = Cultivar.limit(10)
     
    assert_last_resp_coll_parseable_ok('Cultivar', expected.count)
    expected.all.each_with_index{|exp,idx|
      assert_not_expanded_entity(exp, @jresd_coll[idx], :breeder)
      assert_not_expanded_entity(exp, @jresd_coll[idx], :institute)

    }    
    
  end

end
