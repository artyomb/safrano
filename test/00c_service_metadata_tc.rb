
require_relative './test_chinook_db.rb' # Botanics-DB testdata

class ChinookServiceMetadataTC < ChinookDBTest

  def setup
    # run only once.
    # we will just test different parts of the XML result
    # in each test
    #return if @xmlsch
    
    get '/$metadata'

    assert_equal "application/xml;charset=utf-8", 
                 last_response.content_type

    assert last_response.ok?

    @xml = REXML::Document.new(last_response.body)
    
    assert_equal('http://schemas.microsoft.com/ado/2007/06/edmx',
                 @xml.root.attributes['xmlns:edmx'])
                 
    assert_equal('Edmx', @xml.root.name)
    assert_equal('edmx', @xml.root.prefix)
    
    @xmlds = @xml.root.elements.first
    assert_equal 'DataServices', @xmlds.name
    
    @xmlsch = @xmlds.elements.first
    assert_equal 'Schema', @xmlsch.name
    
    

  end

# testing for DateTime fields taht were not exported (bug in version 0.5.2)
  def test_it_has_metadata_DateTimeype

    
    assert employeeTypeXml = @xmlsch.elements.find('EntityType'){|e| 
      e.attributes['Name'] == 'Employee'
    }

    employeeProps = { EmployeeId: 'Edm.Int32',
                      LastName: 'Edm.String',
                      BirthDate: 'Edm.DateTime',
                      HireDate: 'Edm.DateTime',
                      ReportsTo: 'Edm.Int32'}
    
    employeeProps.each{|name, type| 
      assert employeeTypeXml.each_element('Property'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
            (   e.attributes['Type'] == type ) )
      }, "#{name}, #{type} not ok"

    }
    
    assert employeeTypeXml.each_element('Key'){}.find{|e|
      e.each_element('PropertyRef'){}.find{|k| k.attributes['Name'] == 'EmployeeId' }
    } 

    
  end
# testing for Numeric fields taht were not exported (bug in version 0.5.2)
# (should be Edm.Decimal in Odata V2)
  def test_it_has_metadata_Numeric

    
    assert invlineTypeXml = @xmlsch.elements.find('EntityType'){|e| 
      e.attributes['Name'] == 'InvoiceItem'
    }

    invlineProps = { InvoiceLineId: 'Edm.Int32',
                      InvoiceId: 'Edm.Int32',
                      TrackId: 'Edm.Int32',
                      UnitPrice: 'Edm.Decimal(10,2)',
                      Quantity: 'Edm.Int32'}
    
    invlineProps.each{|name, type| 
      assert invlineTypeXml.each_element('Property'){}.find{|e| 
            ( ( e.attributes['Name'] == name.to_s )  and 
            (   e.attributes['Type'] == type ) )
      }, "#{name}, #{type} not ok"

    }
    
    assert invlineTypeXml.each_element('Key'){}.find{|e|
      e.each_element('PropertyRef'){}.find{|k| k.attributes['Name'] == 'InvoiceLineId' }
    } 

    
  end  
 
end

