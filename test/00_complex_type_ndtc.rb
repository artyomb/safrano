#!/usr/bin/env ruby

require 'test/unit'

require_relative '../lib/safrano.rb'


X = Safrano.ComplexType(:i => Integer, :d => String)
Y = Safrano.ComplexType(:i => Integer, :x => X )

class TC_Complex_Type < TCWithoutDB

  def test_class
    
    
    assert_equal Safrano::ComplexType, X.superclass
    assert X.respond_to? :props
    assert_equal( { :i => Integer, :d => String }, X.props)
    assert_equal( { :i => Integer, :x => X }, Y.props)
    
  end
  
  def test_instance
    
    x = X.new(1)
    assert_kind_of Safrano::ComplexType, x
    assert_equal 1, x.i
    
    x.i = 2
    assert_equal 2, x.i
    
  end
  
end
