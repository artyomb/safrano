# frozen_string_literal: true

# needed for ruby < 2.5
module Safrano
  module CoreExt
    module Dir
      module Iter
        unless ::Dir.respond_to? :each_child
          def each_child(dir)
            ::Dir.foreach(dir) do |x|
              next if (x == '.') || (x == '..')

              yield x
            end
          end
        end
      end
    end
  end
end
