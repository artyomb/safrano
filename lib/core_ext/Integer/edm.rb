# frozen_string_literal: true

module Safrano
  module CoreExt
    module Integer
      module Edm
        def type_name
          'Edm.Int32'
        end
      end
    end
  end
end
