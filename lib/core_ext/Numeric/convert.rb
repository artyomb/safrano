# frozen_string_literal: true

# require 'bigdecimal/util'

module Safrano
  module CoreIncl
    module Numeric
      module Convert
        def toDecimalString
          BigDecimal(to_s).to_s('F')
        end

        def toDecimalPrecisionString(precision)
          p = Integer(precision)
          BigDecimal(self, p).to_s('F')
        end

        def toDecimalPrecisionScaleString(precision, scale)
          p = Integer(precision)
          format("%#{p + 2}.#{scale}f", self).strip
        end
      end
    end
  end
end
