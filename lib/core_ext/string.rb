require_relative 'String/edm'
require_relative 'String/convert'

String.extend Safrano::CoreExt::String::Edm
String.include Safrano::CoreIncl::String::Convert
