require_relative 'Date/format'
require 'date'

Date.include Safrano::CoreIncl::Date::Format
Date.extend Safrano::CoreExt::Date::Format
