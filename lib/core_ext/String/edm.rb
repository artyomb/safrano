# frozen_string_literal: true

module Safrano
  module CoreExt
    module String
      module Edm
        def type_name
          'Edm.String'
        end
      end
    end
  end
end
