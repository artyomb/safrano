# frozen_string_literal: true

module Safrano
  module CoreIncl
    module String
      module Convert
        # thanks https://stackoverflow.com/questions/1448670/ruby-stringto-class
        def constantize
          names = split('::')
          names.shift if names.empty? || names.first.empty?

          const = Object
          names.each do |name|
            const = if const.const_defined?(name)
                      const.const_get(name)
                    else
                      const.const_missing(name)
                    end
          end
          const
        end
      end
    end
  end
end
