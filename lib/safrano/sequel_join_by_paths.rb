# frozen_string_literal: true

require_relative '../sequel/plugins/join_by_paths'

Sequel::Model.plugin Sequel::Plugins::JoinByPaths
