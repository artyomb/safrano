# frozen_string_literal: true

require 'rack'
require_relative '../odata/walker'
require_relative 'request'
require_relative 'response'

module Safrano
  # Note there is a strong 1 to 1 relation between an app instance
  # and a published service. --> actually means also
  # we only support on service per App-class because publishing is
  # made on app class level
  class ServerApp
    def initialize
      # just get back the service base instance object
      # that was saved on class level and save it here
      # so it's not needed to call self.class.service
      @service_base = self.class.get_service_base
    end

    def call(env)
      Safrano::Request.new(env, @service_base).process
    end

    #  needed for testing only ? try to remove this
    def self.enable_batch
      @service_base.enable_batch
    end

    # needed for testing only ? try to remove this
    def self.path_prefix(path_pr)
      @service_base.path_prefix path_pr
    end

    # needed for testing only ? try to remove this
    def self.get_service_base
      @service_base
    end

    def self.set_servicebase(sbase)
      @service_base = sbase
      @service_base.enable_v1_service
      @service_base.enable_v2_service
    end

    def self.publish_service(&block)
      sbase = Safrano::ServiceBase.new
      sbase.instance_eval(&block) if block_given?
      sbase.finalize_publishing
      # save published service base instance on App-Class level
      set_servicebase(sbase)
    end
  end
end
