# frozen_string_literal: true

require 'odata/error'

require_relative 'filter/parse'
require_relative 'filter/sequel'

# filter base class and subclass in our OData namespace
module Safrano
  class FilterBase
    # re-useable empty filtering (idempotent)
    EmptyFilter = new.freeze

    def self.factory(filterstr)
      filterstr.nil? ? EmptyFilter : FilterByParse.new(filterstr)
    end

    def apply_to_dataset(dtcx)
      Contract.valid(dtcx)
    end

    # finalize
    def finalize(_jh)
      Contract::OK
    end

    def empty?
      true
    end

    def parse_error?
      false
    end
  end

  # should handle everything by parsing
  class FilterByParse < FilterBase
    attr_reader :filterstr

    def initialize(filterstr)
      @filterstr = filterstr.dup
      @ast = Safrano::Filter::Parser.new(@filterstr).build
    end

    # this build's up the Sequel Filter Expression, and as a side effect,
    # it also finalizes the join helper that we need for the start dataset join
    # the join-helper is shared by the order-by object and was potentially already
    # partly built on order-by object creation.
    def finalize(jh)
      @filtexpr = @ast.if_valid { |ast| ast.sequel_expr(jh) }
    end

    def apply_to_dataset(dtcx)
      #     normally finalize is called before, and thus @filtexpr is set
      @filtexpr.map_result! { |f| dtcx.where(f) }
    end

    # Note: this is really only *parse* error, ie the error encounterd while
    # trying to build the AST
    # Later when evaluating the AST, there can be other errors, they shall
    # be tracked with @error
    def parse_error?
      @ast.error
    end

    def empty?
      false
    end
  end
end
