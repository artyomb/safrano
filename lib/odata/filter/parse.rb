# frozen_string_literal: true

require_relative './token'
require_relative './tree'
require_relative './error'

# top level Safrano namespace
module Safrano
  # for handling $filter
  module Filter
    # Parser for $filter input
    class Parser
      include Token
      attr_reader :cursor
      attr_reader :error

      def initialize(input)
        @tree = RootTree.new
        @cursor = @tree
        @input = input
        @stack = []
        @binop_stack = []
      end

      def server_error
        (@error = ::Safrano::ServerError)
      end

      def grow_at_cursor(child)
        return server_error if @cursor.nil?

        @cursor.attach(child).tap_error { |err| return (@error = err) }
        @cursor = child
      end

      def cursor_at_parent
        @cursor = @cursor.parent
      end

      # detach cursor from parent and move cursor to the parent
      # return the detached subtree
      def detach_cursor
        left = @cursor
        cursor_at_parent
        @cursor.detach(left)
      end

      def insert_before_cursor(node)
        left = detach_cursor
        grow_at_cursor(node)
        @cursor.attach(left).tap_error { |err| return (@error = err) }
      end

      def invalid_separator_error(tok, typ)
        @error = ErrorInvalidSeparator.new(tok, typ, @cursor)
      end

      def unmatched_quote_error(tok, typ)
        @error = UnmatchedQuoteError.new(tok, typ, @cursor)
      end

      def invalid_closing_delimiter_error(tok, typ)
        @error = ErrorUnmatchedClose.new(tok, typ, @cursor)
      end

      def with_accepted(tok, typ)
        (err = @cursor.accept?(tok, typ)) ? (@error = err) : yield
      end

      def build
        each_typed_token(@input) do |tok, typ|
          case typ
          when :FuncTree
            with_accepted(tok, typ) { grow_at_cursor(FuncTree.new(tok)) }

          when :Delimiter
            case tok
            when '('
              with_accepted(tok, typ) do
                grow_at_cursor(IdentityFuncTree.new) unless @cursor.is_a? FuncTree
                unless @error
                  openarg = ArgTree.new('(')
                  @stack << openarg
                  grow_at_cursor(openarg)
                end
              end

            when ')'
              break invalid_closing_delimiter_error(tok, typ) unless (@cursor = @stack.pop)

              with_accepted(tok, typ) do
                @cursor.update_state(tok, typ)
                cursor_at_parent
              end
            end

          when :Separator
            break invalid_separator_error(tok, typ) unless (@cursor = @stack.last)

            with_accepted(tok, typ) { @cursor.update_state(tok, typ) }

          when :UnopTree
            unoptr = UnopTree.new(tok)
            if (prev = @binop_stack.last)
              # handling of lower precedence binding vs the other
              # ones(le,gt,eq...)
              unless prev.precedence < unoptr.precedence
                @cursor = @binop_stack.pop
                @binop_stack << unoptr
              end
            else
              @binop_stack << unoptr
            end
            grow_at_cursor(unoptr)

          when :BinopBool
            with_accepted(tok, typ) do
              binoptr = BinopBool.new(tok)
              if (prev = @binop_stack.last)
                # handling of lower precedence binding vs the other
                # ones(le,gt,eq...)
                unless prev.precedence < binoptr.precedence
                  @cursor = @binop_stack.pop
                  @binop_stack << binoptr
                end
              else
                @binop_stack << binoptr
              end
              insert_before_cursor(binoptr)
            end

          when :BinopArithm
            with_accepted(tok, typ) do
              binoptr = BinopArithm.new(tok)
              if (prev = @binop_stack.last)
                # handling of lower precedence binding vs the other
                # ones(le,gt,eq...)
                unless prev.precedence < binoptr.precedence
                  @cursor = @binop_stack.pop
                  @binop_stack << binoptr
                end
              else
                @binop_stack << binoptr
              end
              insert_before_cursor(binoptr)
            end

          when :Literal
            with_accepted(tok, typ) do
              @cursor.update_state(tok, typ)
              grow_at_cursor(Literal.new(tok))
            end

          when :NullLiteral
            with_accepted(tok, typ) do
              @cursor.update_state(tok, typ)
              grow_at_cursor(NullLiteral.new(tok))
            end

          when :Qualit
            with_accepted(tok, typ) do
              @cursor.update_state(tok, typ)
              grow_at_cursor(Qualit.new(tok))
            end

          when :QString
            with_accepted(tok, typ) do
              @cursor.update_state(tok, typ)
              grow_at_cursor(QString.new(tok))
            end

          when :FPNumber
            with_accepted(tok, typ) do
              @cursor.update_state(tok, typ)
              grow_at_cursor(FPNumber.new(tok))
            end
          when :GuidLit
            with_accepted(tok, typ) do
              @cursor.update_state(tok, typ)
              grow_at_cursor(Guid16.new(tok))
            end
          when :DecimalLit
            with_accepted(tok, typ) do
              @cursor.update_state(tok, typ)
              grow_at_cursor(DecimalLit.new(tok))
            end
          when :DateTimeLit
            with_accepted(tok, typ) do
              @cursor.update_state(tok, typ)
              grow_at_cursor(DateTimeLit.new(tok))
            end
          when :DateTimeOffsetLit
            with_accepted(tok, typ) do
              @cursor.update_state(tok, typ)
              grow_at_cursor(DateTimeOffsetLit.new(tok))
            end
          when :unmatchedQuote
            break unmatched_quote_error(tok, typ)

          when :space
            with_accepted(tok, typ) do
              @cursor.update_state(tok, typ)
            end
          else
            server_error
          end
          break(@error) if @error
        end
        (@error = @tree.check_types) unless @error
        @error || Contract.valid(@tree)
      end
    end
  end
end
