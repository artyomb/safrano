# frozen_string_literal: true

module Safrano
  module Filter
    class Parser
      # Input tokenizer
      module Token
        FUNCNAMES = %w[concat substringof endswith startswith length indexof
                       replace substring trim toupper tolower
                       day hour minute month second year
                       round floor ceiling].freeze
        FUNCRGX = FUNCNAMES.join('|')
        NULLRGX = 'null|NULL|Null'
        QSTRINGRGX = /'((?:[^']|(?:'{2}))*)'/.freeze
        BINOBOOL = '[eE][qQ]|[LlgGNn][eETt]|[aA][nN][dD]|[oO][rR]'
        BINOARITHM = '[aA][dD][dD]|[sS][uU][bB]|[mM][uU][lL]|[dD][iI][vV]|[mM][oO][dD]'
        NOTRGX = 'not|NOT|Not'
        FPRGX = '(\d+(?:\.\d+)?(?:e[+-]?\d+)?)[df]?'
        DECIMALRGX = '(\d+(?:\.\d+))[mM]'
        QUALITRGX = '\w+(?:\/\w+)+'
        # datetime'yyyy-mm-ddThh:mm[:ss[.fffffff]]' NOTE: Spaces are not allowed between datetime and quoted portion.
        # datetime is case-insensitive
        DATIRGX = '\d{4}-\d{2}-\d{2}T\d{2}:\d{2}(?:\:\d{2})?(?:\.\d{1,7})?'
        DATETIMERGX = /datetime'(#{DATIRGX}[zZ]?)'/i.freeze
        DATIOFFRGX = /datetimeoffset'(#{DATIRGX}(?:[zZ]|[+-]\d{2}:\d{2}))'/i.freeze
        GUIDRGX = /guid'([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})'/i.freeze
        RGX = /(#{FUNCRGX})|(#{NULLRGX})|([(),])|(#{BINOBOOL})\s+|(#{BINOARITHM})|(#{NOTRGX})|#{QSTRINGRGX}|#{DECIMALRGX}|#{FPRGX}|(#{QUALITRGX})|#{DATETIMERGX}|#{DATIOFFRGX}|#{GUIDRGX}|(\w+)|(')/.freeze

        def each_typed_token(inp)
          typ = nil

          inp.scan(RGX) do |groups|
            groups.each_with_index do |tok, i|
              if tok

                typ = case i
                      when 0
                        :FuncTree
                      when 1
                        :NullLiteral
                      when 2
                        case tok
                        when '(', ')'
                          :Delimiter
                        when ','
                          :Separator
                        end
                      when 3
                        :BinopBool
                      when 4
                        :BinopArithm
                      when 5
                        :UnopTree
                      when 6
                        :QString
                      when 7
                        :DecimalLit
                      when 8
                        :FPNumber
                      when 9
                        :Qualit
                      when 10
                        :DateTimeLit
                      when 11
                        :DateTimeOffsetLit
                      when 12
                        :GuidLit
                      when 13
                        :Literal
                      when 14
                        :unmatchedQuote
                      end
                yield tok, typ
                break
              end
            end
          end
        end
      end
    end
  end
end
