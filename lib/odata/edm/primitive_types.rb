# frozen_string_literal: true

require 'date'

module Safrano
  # Type mapping DB --> Edm
  #  TypeMap = {"INTEGER" => "Edm.Int32" , "TEXT" => "Edm.String",
  # "STRING" => "Edm.String"}
  # Todo: complete mapping... this is just for the most common ones

  # TODO: use Sequel GENERIC_TYPES: -->
  # Constants
  # GENERIC_TYPES = %w'String Integer Float Numeric BigDecimal Date DateTime
  # Time File TrueClass FalseClass'.freeze
  # Classes specifying generic types that Sequel will convert to
  # database-specific types.
  DB_TYPE_STRING_RGX = /\ACHAR\s*\(\d+\)\z/.freeze
  DB_TYPE_NUMDEC_RGX = /\A(NUMERIC|DECIMAL)\s*(\(\s*((\d+)\s*(,\s*(\d+))?)\s*\))?\s*\z/i.freeze
  # thank you rubular
  # Test String: DECIMAL (55,2 )
  # Match groups
  # 1	DECIMAL
  # 2	(55,2 )
  # 3	55,2
  # 4	55
  # 5	,2
  # 6	2
  DB_TYPE_GUID_RGX = /\A\s*(uuid)\s*\z/i.freeze

  DB_TYPE_FLOATP_RGX = /\A\s*(FLOAT)\s*(\(\s*(\d+)\s*\))?\s*\z/i.freeze

  # Note: "char" (quoted!) is postgresql's byte type
  DB_TYPE_INTLIKE_RGX = /\A\s*(smallserial|smallint|integer|int2|int4|int8|int|mediumint|bigint|serial|bigserial|tinyint)\s*/i.freeze
  # used in $metadata
  # cf. Sequel Database column_schema_default_to_ruby_value
  #                     schema_column_type
  # https://www.odata.org/documentation/odata-version-2-0/overview/

  # type mappings are hard, especially between "Standards" like SQL and OData V2 (might be a bit better in V4 ?)
  # this is all best effort/try to make it work  logic

  RUBY_TY_EDM_TY_MAP = { integer: 'Edm.Int32',
                         string: 'Edm.String',
                         date: 'Edm.DateTime',
                         datetime: 'Edm.DateTime',
                         time: 'Edm.Time',
                         boolean: 'Edm.Boolean',
                         float: 'Edm.Double',
                         decimal: 'Edm.Decimal',
                         blob: 'Edm.Binary' }.freeze
  DB_TY_EDM_TY_MAP = { 'smallint' => 'Edm.Int16',
                       'int2' => 'Edm.Int16',
                       'smallserial' => 'Edm.Int16',
                       'int' => 'Edm.Int32',
                       'integer' => 'Edm.Int32',
                       'serial' => 'Edm.Int32',
                       'mediumint' => 'Edm.Int32',
                       'int4' => 'Edm.Int32',
                       'bigint' => 'Edm.Int64',
                       'bigserial' => 'Edm.Int64',
                       'int8' => 'Edm.Int64',
                       'tinyint' => 'Edm.Byte' }.freeze
  def self.add_edm_types(metadata, props)
    # try num/dec with db_type:
    metadata[:edm_type] = if (md = DB_TYPE_NUMDEC_RGX.match(props[:db_type]))
                            prec = md[4]
                            scale = md[6]
                            if scale && prec
                              if scale == '0' # dont force default scale to 0 like SQL standard
                                metadata[:edm_precision] = prec
                                "Edm.Decimal(#{prec})"
                              else
                                # we have precision and scale
                                metadata[:edm_scale] = scale
                                metadata[:edm_precision] = prec
                                "Edm.Decimal(#{prec},#{scale})"
                              end
                            elsif prec
                              # we have precision only
                              metadata[:edm_precision] = prec
                              "Edm.Decimal(#{prec})"
                            else
                              'Edm.Decimal'
                            end
                          end
    return if metadata[:edm_type]

    # try float(prec) with db_type:
    metadata[:edm_type] = if (md = DB_TYPE_FLOATP_RGX.match(props[:db_type]))
                            # FLOAT( 22) match groups
                            # 1	FLOAT
                            # 2	(22 )
                            # 3	22

                            if (prec = md[3])
                              # we have precision only
                              metadata[:edm_precision] = prec
                              'Edm.Double'
                            end
                          end
    return if metadata[:edm_type]

    # try int-like with db_type:
    # smallint|int|integer|bigint|serial|bigserial

    metadata[:edm_type] = if (md = DB_TYPE_INTLIKE_RGX.match(props[:db_type]))
                            if (itype = md[1])
                              DB_TY_EDM_TY_MAP[itype.downcase]
                            end
                          end
    return if metadata[:edm_type]

    # try Guid with db_type:

    metadata[:edm_type] = if DB_TYPE_GUID_RGX.match(props[:db_type])
                            'Edm.Guid'
                          end

    return if metadata[:edm_type]

    # try with Sequel(ruby) type
    metadata[:edm_type] = RUBY_TY_EDM_TY_MAP[props[:type]]
  end

  # use Edm twice so that we can do include Safrano::Edm and then
  # have Edm::Int32 etc... availabe
  # and we can have Edm::String different from ::String
  module Edm
    module Edm
      module OutputClassMethods
        def type_name
          "Edm.#{name.split('::').last}"
        end

        def odata_collection(array)
          array
        end

        def odata_value(instance)
          instance
        end
      end

      class Null < NilClass
        extend OutputClassMethods
        # nil --> null convertion is done by to_json
        def self.odata_value(_instance)
          nil
        end

        def self.convert_from_urlparam(val)
          return Contract::NOK unless val == 'null'

          Contract.valid(nil)
        end
      end

      # Binary is a String with the BINARY encoding
      class Binary < String
        extend OutputClassMethods

        def self.convert_from_urlparam(val)
          # TODO: this should use base64
          Contract.valid(val.dup.force_encoding('BINARY'))
        end
      end

      # an object alwys evaluates to
      #  true ([true, anything not false & not nil objs])
      #  or false([nil, false])
      class Boolean < Object
        extend OutputClassMethods
        def self.odata_value(instance)
          instance ? true : false
        end

        def self.odata_collection(array)
          array.map { |val| odata_value(val) }
        end

        def self.convert_from_urlparam(val)
          return Contract::NOK unless %w[true false].include?(val)

          Contract.valid(val == 'true')
        end
      end

      # Bytes are usualy represented as Intger in ruby,
      # eg.String.bytes --> Array of ints
      class Byte < Integer
        extend OutputClassMethods

        def self.convert_from_urlparam(val)
          return Contract::NOK unless (bytev = val.to_i) < 256

          Contract.valid(bytev)
        end
      end

      class DateTime < ::DateTime
        extend OutputClassMethods
        def self.odata_value(instance)
          instance.to_datetime
        end

        def self.odata_collection(array)
          array.map { |val| odata_value(val) }
        end

        def self.convert_from_urlparam(val)
          Contract.valid(DateTime.parse(val))
        rescue StandardError
          convertion_error(val)
        end
      end

      class String < ::String
        extend OutputClassMethods

        def self.convert_from_urlparam(val)
          Contract.valid(val)
        end
      end

      class Int32 < Integer
        extend OutputClassMethods

        def self.convert_from_urlparam(val)
          return Contract::NOK unless (ret = number_or_nil(val))

          Contract.valid(ret)
        end
      end

      class Int64 < Integer
        extend OutputClassMethods

        def self.convert_from_urlparam(val)
          return Contract::NOK unless (ret = number_or_nil(val))

          Contract.valid(ret)
        end
      end

      class Double < Float
        extend OutputClassMethods

        def self.convert_from_urlparam(val)
          Contract.valid(val.to_f)
        rescue StandardError
          Contract::NOK
        end
      end

      class Guid < UUIDTools::UUID
        extend OutputClassMethods

        def self.convert_from_urlparam(val)
          Contract::NOK unless (m = Filter::Parser::Token::GUIDRGX.match(val))

          Contract.valid(UUIDTools::UUID.parse(m[1]))
        rescue StandardError
          Contract::NOK
        end
      end
    end
  end
end
