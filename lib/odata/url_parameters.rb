# frozen_string_literal: true

require 'odata/error'

# url parameters processing . Mostly delegates to specialised classes
# (filter, order...) to convert into Sequel exprs.
module Safrano
  class UrlParametersBase
    attr_reader :expand
    attr_reader :select

    # url params validation methods.
    # nil is the expected return for no errors
    # an error class is returned in case of errors
    # this way we can combine multiple validation calls with logical ||
    def check_expand
      @expand.parse_error?
    end

    def check_select
      @select.parse_error?
    end

    def initialize(dataset, params = {})
      @model = if dataset.respond_to? :model
                 dataset.model
               else
                 dataset
               end
      @params = params
    end
  end

  # url parameters for a single entity expand/select
  class UrlParameters4Single < UrlParametersBase
    def initialize(dataset, params)
      super
      @expand = ExpandBase.factory(@params['$expand'], @model)
      @select = SelectBase.factory(@params['$select'], @model)
    end

    def check_all
      return Contract::OK unless @params

      check_expand.if_valid do
        check_select
      end
    end
  end

  # url parameters for a collection expand/select + filter/order
  class UrlParameters4Coll < UrlParametersBase
    attr_reader :filt
    attr_reader :ordby

    def initialize(dataset, params = {})
      super
      # join helper is only needed for odering or filtering
      @jh = @model.join_by_paths_helper if params['$orderby'] || params['$filter']
      @params = params
      @ordby = OrderBase.factory(@params['$orderby'], @jh)
      @filt = FilterBase.factory(@params['$filter'])
      @expand = ExpandBase.factory(@params['$expand'], @model)
      @select = SelectBase.factory(@params['$select'], @model)
    end

    def check_top
      return Contract::OK unless @params['$top']

      itop = number_or_nil(@params['$top'])
      itop.nil? || itop.zero? ? BadRequestError : Contract::OK
    end

    def check_skip
      return Contract::OK unless @params['$skip']

      iskip = number_or_nil(@params['$skip'])
      iskip.nil? || iskip.negative? ? BadRequestError : Contract::OK
    end

    def check_inlinecount
      return Contract::OK unless (icp = @params['$inlinecount'])

      (icp == 'allpages') || (icp == 'none') ? Contract::OK : BadRequestInlineCountParamError
    end

    def check_filter
      (err = @filt.parse_error?) ? err : Contract::OK
    end

    def check_orderby
      return Contract::OK if @ordby.empty?
      return BadRequestOrderParseError if @ordby.parse_error?

      Contract::OK
    end

    def apply_to_dataset(dtcx)
      apply_expand_to_dataset(dtcx).if_valid do |dataset|
        apply_filter_order_to_dataset(dataset)
      end
    end

    def apply_expand_to_dataset(dtcx)
      return Contract.valid(dtcx) if @expand.empty?

      @expand.apply_to_dataset(dtcx)
    end

    # Warning, the @ordby and @filt objects are coupled by way of the join helper
    def apply_filter_order_to_dataset(dtcx)
      return Contract.valid(dtcx) if @filt.empty? && @ordby.empty?

      # filter object and join-helper need to be finalized after filter
      # has been parsed and checked
      @filt.finalize(@jh).if_valid do
        # start with the join
        dtcx = @jh.dataset(dtcx)

        @filt.apply_to_dataset(dtcx).map_result! do |dataset|
          dtcx = dataset
          dtcx = @ordby.apply_to_dataset(dtcx)
          dtcx.select_all(@jh.start_model.table_name)
        end
      end
    end

    ###########################################################
    def check_all
      return Contract::OK unless @params

      # lazy nested proc evaluation.
      # if one check fails, it will be passed up the chain and the ones
      # below will not be evaluated
      check_top.if_valid do
        check_skip.if_valid do
          check_orderby.if_valid do
            check_filter.if_valid do
              check_expand.if_valid do
                check_select.if_valid do
                  check_inlinecount
                end
              end
            end
          end
        end
      end
    end
  end
end
