# frozen_string_literal: true

require_relative 'error'

# our main namespace
module Safrano
  # represents a state transition when navigating/parsing the url path
  # from left to right
  class Transition
    attr_accessor :trans
    attr_accessor :match_result
    attr_accessor :rgx
    attr_reader :remain_idx

    EMPTYSTR = ''
    SLASH = '/'

    RESULT_BAD_REQ_ERR = [nil, :error, ::Safrano::BadRequestError].freeze
    RESULT_NOT_FOUND_ERR = [nil, :error, ::Safrano::ErrorNotFound].freeze
    RESULT_SERVER_TR_ERR = [nil, :error, ServerTransitionError].freeze
    RESULT_END = [nil, :end].freeze

    def initialize(arg, trans: nil, remain_idx: 2)
      @rgx = if arg.respond_to? :each_char
               Regexp.new(arg)
             else
               arg
             end
      @trans = trans
      @remain_idx = remain_idx
    end

    def do_match(str)
      @match_result = @rgx.match(str)
    end

    # remain_idx is the index of the last match-data. ususally its 2
    # but can be overidden
    def path_remain
      @match_result[@remain_idx] if @match_result && @match_result[@remain_idx]
    end

    def path_done
      if @match_result
        @match_result[1] || EMPTYSTR
      else
        EMPTYSTR
      end
    end

    def do_transition(ctx)
      ctx.method(@trans).call(@match_result)
    end
  end

  # Transition that does not move/change the input
  class InplaceTransition < Transition
    def initialize(trans:)
      @trans = trans
    end

    def do_match(str)
      @str = str
    end

    def path_remain
      @str
    end

    def path_done
      EMPTYSTR
    end

    def do_transition(ctx)
      ctx.method(@trans).call(@str)
    end
  end

  TransitionEnd = Transition.new('\A(\/?)\z', trans: 'transition_end')
  TransitionExecuteFunc = InplaceTransition.new(trans: 'transition_execute_func')
  TransitionMetadata = Transition.new('\A(\/\$metadata)(.*)',
                                      trans: 'transition_metadata')
  TransitionBatch = Transition.new('\A(\/\$batch)(.*)',
                                   trans: 'transition_batch')
  TransitionContentId = Transition.new('\A(\/\$(\d+))(.*)',
                                       trans: 'transition_content_id',
                                       remain_idx: 3)
  TransitionCount = Transition.new('(\A\/\$count)(.*)\z',
                                   trans: 'transition_count')
  TransitionValue = Transition.new('(\A\/\$value)(.*)\z',
                                   trans: 'transition_value')
  TransitionLinks = Transition.new('(\A\/\$links)(.*)\z',
                                   trans: 'transition_links')
  attr_accessor :allowed_transitions
end
