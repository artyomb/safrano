## Safrano Documentation v0.6.0  

<  | [up](02_publish.md#safrano-documentation) | [next](03_rack_config.md#safrano-documentation) >

### 2.d OData V2 DateTime and DateTimeOffset 

Up to Safrano 0.5.5 datetime field were wrongly outputed in the iso-8601 format instead of [/Date(ticks)/ format as it is required by OData V2 ](https://www.odata.org/documentation/odata-version-2-0/json-format/). This has been fixed in  Safrano 0.6.0

Note that this format is used in the JSON representation of datetime fields, for GET output but also for POST/PUT input.
DateTime fields in `$filter` expressions must have the `datetime''` literals format as desrcribed in  [the OData V2 Data Model](https://www.odata.org/documentation/odata-version-2-0/overview/#ODataBasics)    

For fields having a Timezone Offset, DateTimeOffset is supported  too;
* in JSON format with /Date(tics+-offset)/ 
* in `$filter` with the `DateTimeOffset''` literal 

### 2.e Timezone considerations

It is well known that Datetime and timezones are a complex topic, and can require quite some work to
get done right. 
We recommend to use UTC on Database level, and do Timezone convertions if required at all on the end-user client side software. Alternatively, if you only have one fixed timezone to support in your end-user application, then you can let Safrano do this convertion (from Database UTC  to "end user" application fixed timezone). Here already starts the complex things: when we say fixed Timezone, we mean a fixed *named* Timezone, like Europe/Berlin or Africa/Casablanca . 

Here are the main Timezone use cases and the corresponding Sequel and Safrano settings (in order of complexity)

#### No Timezone 

Lucky you if your application is local and does not need timezones. In this case you can define the datetime columns on Database level as Timestamp without timezone (or equivalent). Then with `Sequel.default_timezone=nil`  you ensure that Sequel will not do any timezone convertions and that's it.

If your database columns are with timezone for some reason, then you can use `Sequel.default_timezone=:utc` . This will ensure Database AND application timezone is UTC.

#### Single Fixed Timezone

If you want to have your application in a fixed named Timezone, like 'Europe/Berlin', then 
* use Timestamp with timezone columns on DB level
* use these settings
  +  Sequel.database_timezone=:utc
  +  Sequel.application_timezone = 'Europe/Berlin'  or whatever needed


#### Variable Timezones

If your end-user application needs to support multiple different non-UTC timezones, then the recommended way is
* use Timestamp with timezone columns on DB level
* use `Sequel.default_timezone=:utc` . This will ensure Database AND Safrano timezone is UTC
* on end-user client software level, and *only there* convert from UTC to the end-user timezone. 

 
#### Other
While it's technically possible to do other setups, it's not recommended and if you do, be ready to spend some time on it, and eventually to scratch your head on it.
