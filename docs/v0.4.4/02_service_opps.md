## Safrano Documentation v0.4.4 

<  | [up](02_publish.md#safrano-documentation) | [next](03_rack_config.md#safrano-documentation) >

### 2. b. Define and publish service operations

#### Service operations and complex types in Safrano

Service operations are functions that you can expose as
part of your OData service. In Order to be able to define and use freely defined structured data, the OData protocal provides the `Complex type` concept.
We have implemented a minimal yet sufficient `ComplexType` in Safrano.  


#### Define Complex types

*Note:* Excepted otherwise stated, When we say `type` it means a `class`. 

From a functional point of view, `ComplexType` is not so far from Ruby's standard  `Struct`. It's just a convenient way to define 
types (ie. a class) for holding structured data. Like ruby's `Struct`, Safranos ComplexTypes components can be primitives types (eg. String, Integer etc), but they
can be anything else especially they can be made of other ComplexTypes components, ie. ComplexTypes can be deeply nested.


The `Safrano::ComplexType` function generates new `ComplexType` subclasses to hold a set of members and their values. For each member a reader and writer method is created similar to Module#attr_accessor.
The `Safrano::ComplexType` generator function expects a list of member-name and member type pairs. 

*Example:*

```ruby
Address = Safrano::ComplexType(street: String, city: String, housenum: Integer)
Customer = Safrano::ComplexType(name: String, address: Address) 

dave = Customer.new(name: "Dave", address: Address.new(street: "main street",
                                                       city: "Paestum",
                                                       housenum: 5 ))
dave.name     #=> "Dave"
dave.address.city #=> "Paestum"
dave.address.class #=> Address 

```

The main difference of Safrano::ComplexType with `Struct` is that we force the use
of keyword arguments in the Safrano::ComplexType generator, and in the `ComplexType`  subclasses constructors as well . This is a bit less flexible as `Struct` but on the other side it avoids us to have to handle a lot uneeded complexity.

Beside that of course  the  Safrano::ComplexType  implementation was made in
order to easily fullfill  OData specific requirements, eg. being able to output type metadata . As a Safrano user you dont need to care about these details, but this is the reason why we had to implement a  Safrano::ComplexType instead of just using plain ruby `Struct`

*Caveat:* Currently there is no typecheck and no automatic casting at all,
so you could create completely inconsistent complextype instances and safrano will not complain. It is *your* responsibility as a service implementor to stay consistent.
Future safrano versions might provide something better.
 
#### Define and publish service operations

OData service operations are strongly typed, therefore we had to provide a 
some simple way of defining the functions signatures, ie. the input parameters names and types and the type of the function result.

Unlike the other OData ressources, service operations are defined and published in a single step. For that the DSL statement `function_import` can be used. This statement takes a single parameter which is the name of the service operation to be defined. This  `function_import` can then be chained with method calls that define the signature of the function:
* `input` that takes a list of parameter-name and parameter-type pairs (similarly to the ComplexType generator and constructors)
* `return` or `return_collection` to specify the type of the expected function result.
Finally you can end the definition of your function by providing the actual code that needs to be executed when this service operation is called. This can be done by passing a code block to the `return` ( or `return_collection` ) statement. The block should have arguments that match the  `input` definition.

If this all sounds a bit unclear, maybe you can check the examples below

#### Examples

A typical use case would look like that:

```ruby
### Define complex types used in the sevice ops


  ComplexType00 = Safrano::ComplexType(:x => Integer, :d => String )
  ComplexType01 = Safrano::ComplexType(:a => Integer, :b => ComplexType00 )

  publish_service do
   
    # complex types
    publish_complex_type ComplexType00
    publish_complex_type ComplexType01
    
    
    # takes three parameters x, d and a and return them in a nested ComplexType 
    function_import('tfunc')
      .input(:x => Integer, :d => String, :a => Integer)
      .return(ComplexType01) do | x:, d:, a: |
        c00 = ComplexType00.new(x, d)
        ComplexType01.new(a, c00)
    end
    
    # multiplication...
    function_import('vfunc')
      .input(:i => Edm::Int32, :f => Edm::Double)
      .return(Edm::Double) {  | f:, i: |  i*f }
    
    # return an collection of strings
    function_import('fcount')
      .input(:i => Edm::Int32)
      .return_collection(Edm::String) {  |  i: | (0..i).map{|n| n.to_s }   }
      
    # return  a collection of complex types
    function_import('ctfunc')
      .input(:i => Integer, d: Edm::String )
      .return_collection(ComplexType00) do | i:, d: |
        dd = ''
        (0..i).map{|j| dd << d; ComplexType00.new(j, dd.dup ) }
    end  

    # return a Sequel Model instance, ie an `entity`   
    function_import('first_cultivar_func')
      .return(Cultivar) do   Cultivar.first    end  
    
    # return a collection of Sequel Model instances     
    function_import('top10_cultivar_func')
      .return_collection(Cultivar) do
        
        Cultivar.limit(10)
      end
      
  end

```