## Safrano Documentation v0.5.5

<  | [up](02_publish.md#safrano-documentation) | [next](03_rack_config.md#safrano-documentation) >

### 2. c. Safrano's default type system and how to override it

Safrano automatically determines OData/Edm types based on the underlying database types of the published
model columns. The values are then converted accordingly into the corresponding OData json representation.
Safrano tries to stick to the OData standard as much as possible, but we cannot exclude that
some type mapping rules are missing, incomplete, inacurrate or maybe just plain wrong .
In this case, starting from Safrano v0.5.5 you can override the default type mapping and output rules and define your own ones (btw.  you can also report a bug )


#### Safrano's default type system

The following types are supported automatically by Safrano
  + Edm.Binary (sqlite-blob; postgres bytea)
  + Edm.Boolean
  + Edm.Byte (sqlite only)
  + Edm.DateTime
  + Edm.Decimal
  + Edm.Double. Note: Single precision DB colums are mapped to Edm.Double too
  + Edm.Int16, Edm.Int32, Edm.Int64
  + Edm.String

The mapping is made in two steps:
1. Determination based on the model column DB Type
2. Determination based on the model column  ruby (Sequel) type

| DB Type | OData Edm Type  |
|---------|-----------------| 
| NUMERIC,DECIMAL | Edm.Decimal |
| NUMERIC(p),DECIMAL(p) | Edm.Decimal(p) |
| NUMERIC(p,s),DECIMAL(p,s) | Edm.Decimal(p,s) |
| FLOAT | Edm.Double |
| FLOAT(p) | Edm.Double  |
| SMALLINT, INT2, SMALLSERIAL | Edm.Int16 |
| INT, INTEGER, SERIAL, MEDIUMINT, INT4  | Edm.Int32 |
| BIGINT, BIGSERIAL, INT8 | Edm.Int64 |
| TINYINT | Edm.Byte |

| Ruby(Sequel) Type | OData Edm Type  |
|---------|-----------------| 
| integer | Edm.Int32 |
| string | Edm.String |
| date | Edm.DateTime |
| datetime | Edm.DateTime |
| time | Edm.DateTime |
| boolean | Edm.Boolean |
| float | Edm.Double |
| decimal | Edm.Decimal |
| blob | Edm.Binary |

#### Publish settings to override default types system

To override the default mappings, you can use the publish DSL function `with_db_type(db_types){ ... }`
As arguments you can pass a list of db-types for which the statement should be considered and a block defining
further what needs to be done for these db-types . In the code block you can use the two statements
* edm_type  :  the Edm type (as a string) as it should appear in $metadata document 
* json_value : a conversion lambda or proc with one input parameter, the value to be converted. The lambda/proc should return the converted value  

*Example:*

```ruby
### Example how to override default types system
  
  publish_service do

    title  'Type testing OData Example'
    name  'TypeService'
    namespace  'ODataTyx'
    server_url 'http://ODataTyx.org'
    path_prefix '/'


    # lets test user-defined type mapping with
    # postgresql's internal  1-byte "char" type, which is what comes closest to a byte type in pg
    # but OData expects it as a 1-byte int. Thus the convertion is with the String.ord (1-char ordinal)
    with_db_type('"char"') do 
      edm_type 'Edm.Byte'
      json_value ->(val){  val&.ord }
    end
    
    with_db_type('numeric', 'decimal') do 
      edm_type 'Edm.Decimal'
      json_value ->(val){  
        # this is equivalent to Safrano's default decimal output 
        # cf. Safrano::CoreIncl::Numeric::Convert::toDecimalString
        BigDecimal(to_s).to_s('F')
      }
      
      with_one_param{|precision| 
        edm_type "Edm.Decimal(#{precision})"
        json_value ->(val){ 
          # this is equivalent to Safrano's default output 
          # cf. Safrano::CoreIncl::Numeric::Convert::toDecimalPrecisionString(precision)
          p = Integer(precision)
          BigDecimal(val, p).to_s('F')
        }
      }
      
      with_two_params{|precision, scale| 
        edm_type "Edm.Decimal(#{precision},#{scale})"
        json_value ->(val){  
          # this is equivalent to Safrano's default output 
          # cf. Safrano::CoreIncl::Numeric::Convert::toDecimalPrecisionScaleString(precision)
          p = Integer(precision)
          sprintf("%#{p + 2}.#{scale}f", val).strip  
        }
      }
            
    end
 
  
    publish_model XYFoo
    publish_model YZBarEtc
    
         
  end

```
